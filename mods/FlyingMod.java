import org.darkstorm.minecraft.darkmod.DarkMod;
import org.darkstorm.minecraft.darkmod.events.*;
import org.darkstorm.minecraft.darkmod.hooks.client.*;
import org.darkstorm.minecraft.darkmod.hooks.client.packets.*;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.minecraft.darkmod.mod.commands.*;
import org.darkstorm.minecraft.darkmod.mod.util.constants.ChatColor;
import org.darkstorm.minecraft.darkmod.tools.*;
import org.darkstorm.tools.events.*;
import org.darkstorm.tools.strings.StringTools;
import org.lwjgl.input.Keyboard;

public class FlyingMod extends Mod implements EventListener, CommandListener {
	private DarkMod darkMod = DarkMod.getInstance();
	private boolean enableHovering = false;
	private boolean lastPressed = false;
	private FlyMode mode = FlyMode.NORMAL;
	private double flySpeed = 0.5;
	private NoFallMode noFall = null;

	private enum FlyMode {
		NORMAL,
		BEDFLY,
		BEDFLY_NOPATCH
	}

	private enum NoFallMode {
		NORMAL,
		NOCHEAT
	}

	private int nextHit = 0, packetSkips = 0;

	@Override
	public String getName() {
		return "Flying Mod";
	}

	@Override
	public String getShortDescription() {
		return "IT'S A BIRD! NO, IT'S A PLANE! NO, IT'S "
				+ darkMod.getUsername()
				+ "!!! Hold CTRL + Space to ascend, Shift + Space to descend, hold C or press CTRL + Shift + C to hover.";
	}

	@Override
	public String getFullDescription() {
		return "<html>Allows you to <b>FLY!</b><br />"
				+ "Hold CTRL + Space to fly, hold C to hover.<br />"
				+ "On SSP and SMP, you may land from any height and not die, "
				+ "unless the SMP server has NoCheat, then you will get damaged. Falling "
				+ "from heigher than 15 blocks is sure-death without armor. "
				+ "Armor does assist in decreasing damage done when falling."
				+ "</html>";
	}

	@Override
	public ModControl getControlOption() {
		return ModControl.TOGGLE;
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public void onStart() {
		eventManager.addListener(TickEvent.class, this);
		eventManager.addListener(PacketEvent.class, this);
		commandManager.registerListener(new Command("flymode",
				"/flymode <normal|bedfly>", "Makes you fly"), this);
		commandManager.registerListener(new Command("nofall",
				"/nofall <normal|nocheat|off>", "Sets no-fall mode"), this);
		commandManager.registerListener(new Command("flyspeed",
				"/flyspeed <speed>", "Sets the flying speed. Default is 0.5"),
				this);

	}

	@Override
	public void onStop() {
		eventManager.removeListener(TickEvent.class, this);
		eventManager.removeListener(PacketEvent.class, this);
		commandManager.unregisterListener("flymode");
		commandManager.unregisterListener("nofall");
		commandManager.unregisterListener("flyspeed");
	}

	@Override
	public int loop() {
		return 500;
	}

	@Override
	public synchronized void onEvent(Event event) {
		if(event instanceof TickEvent) {
			Player player = minecraft.getPlayer();
			MultiplayerWorld world = minecraft.getWorld();
			if(player != null && world != null) {
				if(player.equals(minecraft.getPlayer())) {
					switch(mode) {
					case BEDFLY:
					case BEDFLY_NOPATCH:
						NetworkHandler networkHandler = world
								.getNetworkHandler();
						if(nextHit <= 0) {
							int playerID = player.getID();
							if(mode == FlyMode.BEDFLY) {
								Packet13PlayerMoveRotate movePacket = ReflectionUtil
										.instantiate(ClassRepository
												.getClassForInterface(Packet13PlayerMoveRotate.class));
								movePacket.setX(player.getX());
								movePacket.setStance(player.getY());
								movePacket.setZ(player.getZ());
								movePacket.setRotationX(player.getRotationX());
								movePacket.setRotationY(player.getRotationY());
								movePacket
										.setY(player.getY() - 1.62000000476837);
								networkHandler.sendPacket(movePacket);
							}

							Packet19EntityAction actionPacket = ReflectionUtil
									.instantiate(ClassRepository
											.getClassForInterface(Packet19EntityAction.class));
							actionPacket.setEntityID(playerID);
							actionPacket.setState(3);
							networkHandler.sendPacket(actionPacket);

							if(mode == FlyMode.BEDFLY) {
								Packet13PlayerMoveRotate movePacket = ReflectionUtil
										.instantiate(ClassRepository
												.getClassForInterface(Packet13PlayerMoveRotate.class));
								movePacket.setX(player.getX());
								movePacket.setStance(player.getY());
								movePacket.setZ(player.getZ());
								movePacket.setRotationX(player.getRotationX());
								movePacket.setRotationY(player.getRotationY());
								movePacket
										.setY(player.getY() - 1.62000000476837);
								networkHandler.sendPacket(movePacket);
							}

							System.out.println("Fly: (" + player.getX() + ", "
									+ player.getY() + ", " + player.getZ()
									+ ")");

							packetSkips = 3;
							nextHit = 13;
						} else
							nextHit--;
						networkHandler
								.sendPacket(ReflectionUtil.instantiate(ClassRepository
										.getClassForInterface(Packet0KeepAlive.class)));
					case NORMAL:
						setSpeed();
					}
				}
				if(noFall == NoFallMode.NORMAL) {
					player.setFallDistance(0);
				} else if(noFall == NoFallMode.NOCHEAT) {
					double d = player.getFallDistance() > 18.0F ? 1.7D : 0.61D;
					if(player.getSpeedY() <= -d) {
						Packet19EntityAction action = ReflectionUtil
								.instantiate(ClassRepository
										.getClassForInterface(Packet19EntityAction.class));
						action.setEntityID(player.getID());
						action.setState(3);
						NetworkHandler networkHandler = world
								.getNetworkHandler();
						networkHandler.sendPacket(action);
					}
				}
				if(spaceLastPressed)
					player.setOnGround(true);
			}
		} else if(event instanceof PacketEvent) {
			if(noFall != null) {
				PacketEvent packetEvent = (PacketEvent) event;
				Packet packet = packetEvent.getPacket();
				if(packet instanceof Packet10PlayerPosition)
					((Packet10PlayerPosition) packet).setOnGround(true);
			}
			if(mode == FlyMode.BEDFLY) {
				PacketEvent packetEvent = (PacketEvent) event;
				if(packetEvent instanceof PacketReceivedEvent
						&& packetEvent.getPacket() instanceof Packet13PlayerMoveRotate) {
					Packet13PlayerMoveRotate packet = (Packet13PlayerMoveRotate) packetEvent
							.getPacket();
					Player player = minecraft.getPlayer();
					if(player != null) {
						double distance = getDistanceTo(player, packet.getX(),
								packet.getY(), packet.getZ());
						if(packetSkips > 0 && distance < 2) {
							Packet13PlayerMoveRotate movePacket = ReflectionUtil
									.instantiate(ClassRepository
											.getClassForInterface(Packet13PlayerMoveRotate.class));
							movePacket.setX(packet.getX());
							movePacket.setStance(packet.getY());
							movePacket.setZ(packet.getZ());
							movePacket.setRotationX(packet.getRotationX());
							movePacket.setRotationY(packet.getRotationY());
							movePacket.setY(packet.getStance());
							minecraft.getWorld().getNetworkHandler()
									.sendPacket(movePacket);
							packetSkips--;
							System.out.println("Teleport: (" + packet.getX()
									+ ", " + packet.getY() + ", "
									+ packet.getZ() + ", " + packet.getStance()
									+ ")");
							packet.setStance(player.getY()
									+ (packet.getStance() - packet.getY()));
							packet.setX(player.getX());
							packet.setY(player.getY());
							packet.setZ(player.getZ());
							packet.setRotationX(player.getRotationX());
							packet.setRotationY(player.getRotationY());
						}
					}
				}
			}

			// if(distance < 2) {
			// packetSkips--;
			// Packet13PlayerMoveRotate movementPacket =
			// (Packet13PlayerMoveRotate) ReflectionUtil
			// .instantiate(ClassRepository
			// .getClassForInterface(Packet13PlayerMoveRotate.class));
			// movementPacket.setX(packet.getX());
			// movementPacket.setY(packet.getStance());
			// movementPacket.setZ(packet.getZ());
			// movementPacket.setRotationX(packet.getRotationX());
			// movementPacket.setRotationY(packet.getRotationY());
			// movementPacket.setStance(packet.getY());
			// System.out.println("Packet Y: " + packet.getY()
			// + " Packet Stance: " + packet.getStance());
			// minecraft.getWorld().getNetworkHandler()
			// .sendPacket(movementPacket);
			// packet.setX(player.getX());
			// packet.setStance(player.getY()
			// + (packet.getStance() - packet.getY()));
			// System.out.println("Player Y: " + packet.getY()
			// + " Stance: " + packet.getStance());
			// packet.setY(player.getY());
			// packet.setZ(player.getZ());
			// packet.setRotationX(player.getRotationX());
			// packet.setRotationY(player.getRotationY());
			// packet.setOnGround(player.isOnGround());
			// }
			// }
			// }
			// }
		}
	}

	private boolean spacePressed = false;
	private boolean spaceLastPressed = false;

	private void setSpeed() {
		Player player = minecraft.getPlayer();
		spaceLastPressed = false;
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
			if(!spacePressed) {
				spacePressed = true;
				spaceLastPressed = true;
			}
		} else if(spacePressed)
			spacePressed = false;
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)
				&& Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)) {
			player.setSpeedY(flySpeed);
		} else if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)
				&& Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			player.setSpeedY(-flySpeed);
		} else if(Keyboard.isKeyDown(Keyboard.KEY_C) || enableHovering)
			player.setSpeedY(0);
		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			player.setOnGround(true);
			player.setFallDistance(0);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_C)
				&& Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)
				&& Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) && !lastPressed) {
			enableHovering = !enableHovering;
			lastPressed = true;
		} else if((!Keyboard.isKeyDown(Keyboard.KEY_C) || !Keyboard
				.isKeyDown(Keyboard.KEY_LCONTROL)) && lastPressed)
			lastPressed = false;
	}

	@Override
	public void onCommand(String command) {
		String[] parts = command.split(" ");
		if(parts.length != 2)
			return;
		if(parts[0].equalsIgnoreCase("flymode")) {
			if(parts[1].equalsIgnoreCase("normal")) {
				mode = FlyMode.NORMAL;
				displayText(ChatColor.GRAY + "Flying mode set to "
						+ ChatColor.GOLD + "normal");
			} else if(parts[1].equalsIgnoreCase("bedfly")) {
				mode = FlyMode.BEDFLY;
				displayText(ChatColor.GRAY + "Flying mode set to "
						+ ChatColor.GOLD + "bedfly");
			} else if(parts[1].equalsIgnoreCase("bedflync")) {
				mode = FlyMode.BEDFLY_NOPATCH;
				displayText(ChatColor.GRAY + "Flying mode set to "
						+ ChatColor.GOLD + "bedfly without patch");
			} else
				displayText(ChatColor.GRAY + "Unknown flying mode");
		} else if(parts[0].equalsIgnoreCase("flyspeed")
				&& StringTools.isDouble(parts[1])) {
			flySpeed = Double.parseDouble(parts[1]);
			displayText(ChatColor.GRAY + "Flying speed set to "
					+ ChatColor.GOLD + flySpeed);
		} else if(parts[0].equalsIgnoreCase("nofall")) {
			if(parts[1].equalsIgnoreCase("off")) {
				noFall = null;
				displayText(ChatColor.GRAY + "No-Fall is now " + ChatColor.GOLD
						+ "disabled");
			} else if(parts[1].equalsIgnoreCase("normal")) {
				noFall = NoFallMode.NORMAL;
				displayText(ChatColor.GRAY + "No-Fall is now " + ChatColor.GOLD
						+ "normal");
			} else if(parts[1].equalsIgnoreCase("nocheat")) {
				noFall = NoFallMode.NOCHEAT;
				displayText(ChatColor.GRAY + "No-Fall is now " + ChatColor.GOLD
						+ "nocheat");
			} else
				displayText(ChatColor.GRAY + "Unknown no-fall mode");
		}
	}

}
