package org.darkstorm.minecraft.darkmod.tools;

import java.awt.Image;
import java.io.*;
import java.net.URL;

import javax.imageio.ImageIO;

public class Tools {
	public static enum OperatingSystem {
		LINUX,
		SOLARIS,
		WINDOWS,
		MAC,
		UNKNOWN;
	}

	private static long version;

	static {
		String versionString = readVersion();
		try {
			version = Long.valueOf(versionString);
		} catch(Exception exception) {
			version = -1;
		}
	}

	private static String readVersion() {
		File workingDir = getMinecraftDirectory();
		File versionFile = new File(workingDir, "bin/version");
		try {
			DataInputStream inputStream = new DataInputStream(new FileInputStream(versionFile));
			String version = inputStream.readUTF();
			inputStream.close();
			return version;
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	private Tools() {
	}

	public static Image getIcon(String name) {
		try {
			return ImageIO.read(Tools.class.getResourceAsStream("/resources/" + name + ".png"));
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(250);
		} catch(Exception exception) {}
	}

	public static boolean isRunningFromJar() {
		URL jarLocation = Tools.class.getProtectionDomain().getCodeSource().getLocation();
		String jarLocationString = jarLocation != null ? jarLocation.toString() : null;
		return jarLocationString != null && jarLocationString.endsWith(".jar");
	}

	public static File getCurrentJar() {
		return new File(Tools.class.getProtectionDomain().getCodeSource().getLocation().getFile());
	}

	public static long getMinecraftBuild() {
		return version;
	}

	public static File getCurrentDirectory() {
		if(isRunningFromJar())
			return getCurrentJar().getParentFile();
		else if(Tools.class.getProtectionDomain() != null && Tools.class.getProtectionDomain().getCodeSource() != null && Tools.class.getProtectionDomain().getCodeSource().getLocation() != null)
			return new File(Tools.class.getProtectionDomain().getCodeSource().getLocation().getFile()).getParentFile();
		else
			return new File(System.getProperty("user.dir"));
	}

	public static File getMinecraftDirectory() {
		String userHome = System.getProperty("user.home", ".");
		File workingDirectory;
		switch(getPlatform()) {
		case LINUX:
		case SOLARIS:
			workingDirectory = new File(userHome, ".minecraft/");
			break;
		case WINDOWS:
			String applicationData = System.getenv("APPDATA");
			if(applicationData != null)
				workingDirectory = new File(applicationData, ".minecraft/");
			else
				workingDirectory = new File(userHome, ".minecraft/");
			break;
		case MAC:
			workingDirectory = new File(userHome, "Library/Application Support/minecraft");
			break;
		default:
			workingDirectory = new File(userHome, ".minecraft/");
		}
		if((!workingDirectory.exists()) && (!workingDirectory.mkdirs()))
			throw new RuntimeException("The working directory could not be created: " + workingDirectory);
		return workingDirectory;
	}

	public static OperatingSystem getPlatform() {
		String osName = System.getProperty("os.name").toLowerCase();
		if(osName.contains("win"))
			return OperatingSystem.WINDOWS;
		if(osName.contains("mac"))
			return OperatingSystem.MAC;
		if(osName.contains("solaris"))
			return OperatingSystem.SOLARIS;
		if(osName.contains("sunos"))
			return OperatingSystem.SOLARIS;
		if(osName.contains("linux"))
			return OperatingSystem.LINUX;
		if(osName.contains("unix"))
			return OperatingSystem.LINUX;
		return OperatingSystem.UNKNOWN;
	}
}
