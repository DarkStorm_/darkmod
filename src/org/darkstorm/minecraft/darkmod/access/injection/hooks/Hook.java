package org.darkstorm.minecraft.darkmod.access.injection.hooks;

import java.util.*;

import org.darkstorm.minecraft.darkmod.access.injection.Injector;
import org.jdom.Element;

import org.apache.bcel.generic.*;

public abstract class Hook {
	protected Injector injector;
	protected boolean injected;

	private static Map<String, Map<Integer, Integer>> positionUpdates = new HashMap<String, Map<Integer, Integer>>();

	public Hook(Injector injector, Element element) {
		this.injector = injector;
		readElement(element);
	}

	protected abstract void readElement(Element element);

	public void attemptInjection(ClassGen classGen) {
		if(isInjectable(classGen)) {
			inject(classGen);
			injected = true;
		}
	}

	protected abstract boolean isInjectable(ClassGen classGen);

	protected abstract void inject(ClassGen classGen);

	public boolean isInjected() {
		return injected;
	}

	public abstract String getInterfaceName();

	protected void updateMethodPositions(MethodGen method, int position,
			int offset) {
		String key = method.getClassName() + "." + method.getName()
				+ method.getSignature();
		Map<Integer, Integer> positions = positionUpdates.get(key);
		Integer zero = Integer.valueOf(0);
		if(positions == null) {
			positions = new HashMap<Integer, Integer>();
			for(int originalPosition : method.getInstructionList()
					.getInstructionPositions())
				positions.put(originalPosition, zero);
			positionUpdates.put(key, positions);
		}
		Integer originalOffset = positions.get(position);
		if(originalOffset == null)
			originalOffset = zero;
		positions.put(position, originalOffset + offset);
	}

	protected int getOffsetAtPosition(MethodGen method, int position) {
		String key = method.getClassName() + "." + method.getName()
				+ method.getSignature();
		Map<Integer, Integer> positions = positionUpdates.get(key);
		if(positions == null)
			return 0;
		int offset = 0;
		for(Integer pos : positions.keySet()) {
			if(pos.intValue() > position)
				continue;
			offset += positions.get(pos).intValue();
		}
		return offset;
	}
}
