package org.darkstorm.minecraft.darkmod.access.injection;

import java.awt.*;
import java.io.File;
import java.lang.reflect.*;

import org.darkstorm.minecraft.darkmod.DarkMod;
import org.darkstorm.minecraft.darkmod.access.AccessHandler;
import org.darkstorm.minecraft.darkmod.hooks.client.Minecraft;
import org.darkstorm.minecraft.darkmod.tools.*;
import org.darkstorm.minecraft.darkmod.ui.LoginUI;
import org.darkstorm.tools.misc.SysTools;
import org.lwjgl.opengl.Display;

public class InjectionHandler extends AccessHandler {
	private LoginUI loginUI;
	private ClassLoader classLoader;
	private Thread minecraftThread;
	private Minecraft minecraft;
	private String minecraftVersion;

	@Override
	public void load(LoginUI loginUI) {
		this.loginUI = loginUI;
		Injector injector = new Injector(loginUI);
		injector.run();
		classLoader = injector.getClassLoader();
		minecraftVersion = injector.getVersion();
	}

	@Override
	public synchronized void start() {
		if(minecraft != null)
			return;
		try {
			if(loginUI != null) {
				loginUI.setDialogText("Starting Minecraft...");
				loginUI.getDialogProgressBar().setIndeterminate(true);
			}// Display hack
			final DarkMod darkMod = DarkMod.getInstance();
			Canvas canvas = darkMod.getUI().getCanvas();
			Graphics g = canvas.getGraphics();
			if(g != null) {
				g.setColor(Color.BLACK);
				g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
				g.dispose();
			}
			Display.setParent(canvas);

			// Invoke main to start Minecraft
			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Class<?> mainClass = ClassRepository.getClassByName("net.minecraft.client.main.Main");
						Method mainMethod = mainClass.getMethod("main", String[].class);
						File minecraftDir = Tools.getMinecraftDirectory();
						String[] args = new String[] { "--username", darkMod.getUsername(), "--session", darkMod.getSessionID(), "--version", darkMod.getMinecraftVersion(), "--gameDir", minecraftDir.getAbsolutePath(), "--assetsDir", new File(minecraftDir, "assets").getAbsolutePath() };
						mainMethod.invoke(null, (Object) args);
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			});
			thread.setPriority(10);
			thread.start();
			while(!thread.getName().equals("Minecraft main thread"))
				Thread.sleep(50);
			minecraftThread = thread;

			// Extract Minecraft instance from static instance field
			Class<?> minecraftClass = ClassRepository.getClassForInterface(Minecraft.class);
			Field instanceField = null;
			for(Field field : minecraftClass.getDeclaredFields())
				if(Modifier.isPrivate(field.getModifiers()) && Modifier.isStatic(field.getModifiers()) && field.getType().equals(minecraftClass))
					instanceField = field;
			instanceField.setAccessible(true);
			minecraft = (Minecraft) instanceField.get(null);
		} catch(Throwable exception) {
			exception.printStackTrace();
			SysTools.exit("Could not start minecraft", -1);
		}
	}

	@Override
	public Minecraft getMinecraft() {
		return minecraft;
	}

	public Thread getMinecraftThread() {
		return minecraftThread;
	}

	@Override
	public ClassLoader getClassLoader() {
		return classLoader;
	}

	@Override
	public String getMinecraftVersion() {
		return minecraftVersion;
	}

}
