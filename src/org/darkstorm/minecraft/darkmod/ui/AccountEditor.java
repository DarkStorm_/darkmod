package org.darkstorm.minecraft.darkmod.ui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class AccountEditor extends JDialog {
	// JFormDesigner - Variables declaration - DO NOT MODIFY
	// //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JTextField usernameField;
	private JPasswordField passwordField;
	private JPanel buttonBar;
	private JButton cancelButton;
	private JButton saveButton;

	// JFormDesigner - End of variables declaration //GEN-END:variables

	public AccountEditor() {
		initComponents();
	}

	private void cancelButtonActionPerformed(ActionEvent e) {

	}

	private void saveButtonActionPerformed(ActionEvent e) {

	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY
		// //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		JLabel usernameLabel = new JLabel();
		usernameField = new JTextField();
		JLabel passwordLabel = new JLabel();
		passwordField = new JPasswordField();
		buttonBar = new JPanel();
		cancelButton = new JButton();
		saveButton = new JButton();

		// ======== this ========
		setTitle("Edit Account");
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// ======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			dialogPane.setLayout(new BorderLayout());

			// ======== contentPanel ========
			{
				contentPanel.setLayout(new GridBagLayout());
				((GridBagLayout) contentPanel.getLayout()).columnWidths = new int[] {
						0, 0, 0 };
				((GridBagLayout) contentPanel.getLayout()).rowHeights = new int[] {
						0, 0, 0 };
				((GridBagLayout) contentPanel.getLayout()).columnWeights = new double[] {
						0.0, 1.0, 1.0E-4 };
				((GridBagLayout) contentPanel.getLayout()).rowWeights = new double[] {
						0.0, 0.0, 1.0E-4 };

				// ---- usernameLabel ----
				usernameLabel.setText("Username:");
				contentPanel.add(usernameLabel, new GridBagConstraints(0, 0, 1,
						1, 0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
				contentPanel.add(usernameField, new GridBagConstraints(1, 0, 1,
						1, 0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

				// ---- passwordLabel ----
				passwordLabel.setText("Password:");
				contentPanel.add(passwordLabel, new GridBagConstraints(0, 1, 1,
						1, 0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
				contentPanel.add(passwordField, new GridBagConstraints(1, 1, 1,
						1, 0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			// ======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(10, 0, 0, 0));
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] {
						0, 85, 80 };
				((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] {
						1.0, 0.0, 0.0 };

				// ---- cancelButton ----
				cancelButton.setText("Cancel");
				cancelButton.setIcon(new ImageIcon(getClass().getResource(
						"/resources/cancel.png")));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelButtonActionPerformed(e);
					}
				});
				buttonBar.add(cancelButton, new GridBagConstraints(1, 0, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

				// ---- saveButton ----
				saveButton.setText("Save");
				saveButton.setIcon(new ImageIcon(getClass().getResource(
						"/resources/apply.png")));
				saveButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						saveButtonActionPerformed(e);
					}
				});
				buttonBar.add(saveButton, new GridBagConstraints(2, 0, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		setSize(295, 120);
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization
		// //GEN-END:initComponents
	}
}
