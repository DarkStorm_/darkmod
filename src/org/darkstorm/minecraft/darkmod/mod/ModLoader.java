package org.darkstorm.minecraft.darkmod.mod;

import java.io.File;
import java.net.*;

import org.darkstorm.minecraft.darkmod.tools.Tools;
import org.darkstorm.tools.strings.StringTools;

public class ModLoader {
	private final ModHandler modHandler;

	private URLClassLoader classLoader;

	ModLoader(ModHandler modHandler) {
		this.modHandler = modHandler;
	}

	public void reloadMods() {
		try {
			File modsDir;
			if(Tools.isRunningFromJar()) {
				File jar = Tools.getCurrentJar();
				File currentDirectory = jar.getParentFile();
				modsDir = new File(currentDirectory, "mods");
			} else
				modsDir = new File(Tools.getCurrentDirectory(), "mods");
			modsDir = modsDir.getCanonicalFile();
			System.out.println("Mods directory: " + modsDir.getCanonicalPath());
			if(!modsDir.exists())
				modsDir.mkdir();
			URL modsDirURL = modsDir.toURI().toURL();
			modHandler.clearMods();
			if(classLoader != null) {
				classLoader.close();
				classLoader = null;
			}
			System.gc();
			classLoader = new URLClassLoader(new URL[] { modsDirURL }, getClass().getClassLoader());
			loadDirectory(classLoader, modsDir);
		} catch(Exception exception) {
			exception.printStackTrace();
		}
	}

	private void loadDirectory(ClassLoader classLoader, File directory) {
		for(File file : directory.listFiles()) {
			try {
				if(file.isDirectory()) {
					loadDirectory(classLoader, file);
					continue;
				}
				String fileName = file.getName();
				if(!fileName.endsWith(".class") || fileName.contains("$"))
					continue;
				String path = file.getAbsolutePath();
				path = StringTools.splitFirst(path, "mods" + File.separator)[1].replace(File.separatorChar, '.').replace(".class", "");
				Class<?> modClass = classLoader.loadClass(path);
				if(!Mod.class.isAssignableFrom(modClass))
					continue;
				Mod mod = (Mod) modClass.newInstance();
				modHandler.addMod(mod);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	public ModHandler getModHandler() {
		return modHandler;
	}
}
