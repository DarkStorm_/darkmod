package org.darkstorm.minecraft.darkmod.access.injection.hooks;

import org.darkstorm.minecraft.darkmod.access.injection.Injector;
import org.darkstorm.minecraft.darkmod.events.CallbackEvent;
import org.jdom.Element;

import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.*;

public class CallbackHook extends Hook {
	private String className;
	private String interfaceName;
	private String method;
	private int position;
	private String callbackMethod;
	private String[] values;
	private int cancelTarget;
	private int callEventTarget;
	private int storeVar;

	public CallbackHook(Injector injector, Element element) {
		super(injector, element);
	}

	public String getClassName() {
		return className;
	}

	@Override
	public String getInterfaceName() {
		return interfaceName;
	}

	public String getMethod() {
		return method;
	}

	public int getPosition() {
		return position;
	}

	public String getCallbackMethod() {
		return callbackMethod;
	}

	public int getCancelTarget() {
		return cancelTarget;
	}

	public int getCallEventTarget() {
		return callEventTarget;
	}

	public int getStoreVar() {
		return storeVar;
	}

	public String[] getValues() {
		String[] values = new String[this.values.length];
		System.arraycopy(this.values, 0, values, 0, values.length);
		return values;
	}

	@Override
	protected void readElement(Element element) {
		className = element.getAttributeValue("class");
		interfaceName = element.getAttributeValue("interface");
		method = element.getAttributeValue("method");
		position = Integer.parseInt(element.getAttributeValue("position"));
		callbackMethod = element.getAttributeValue("callback");
		String valuesAttribute = element.getAttributeValue("values");
		if(valuesAttribute != null) {
			String[] values = valuesAttribute.split(",");
			if(values.length != 1 || !values[0].isEmpty())
				this.values = values;
		} else
			values = new String[0];
		String cancelAttribute = element.getAttributeValue("cancel");
		if(cancelAttribute != null)
			cancelTarget = Integer.parseInt(cancelAttribute);
		else
			cancelTarget = -1;
		String callAttribute = element.getAttributeValue("call");
		if(callAttribute != null)
			callEventTarget = Integer.parseInt(callAttribute);
		else
			callEventTarget = -1;
		String storeAttribute = element.getAttributeValue("store");
		if(storeAttribute != null)
			storeVar = Integer.parseInt(storeAttribute);
		else
			storeVar = -1;

	}

	@Override
	protected boolean isInjectable(ClassGen classGen) {
		return className.equals(classGen.getClassName());
	}

	@Override
	protected void inject(ClassGen classGen) {
		String methodName = method.split("\\(")[0];
		String methodSignature = method.substring(methodName.length());
		Method originalMethod = null;
		MethodGen methodForCallback = null;
		for(Method method : classGen.getMethods()) {
			if(methodName.equals(method.getName())
					&& methodSignature.equals(method.getSignature())) {
				originalMethod = method;
				methodForCallback = new MethodGen(method, className,
						classGen.getConstantPool());
			}
		}
		InstructionList instructionList = methodForCallback
				.getInstructionList();
		InstructionFactory factory = new InstructionFactory(classGen);
		String callbackMethodName = callbackMethod.split("\\(")[0];
		String callbackMethodSignature = callbackMethod
				.substring(callbackMethodName.length());
		int offset = getOffsetAtPosition(methodForCallback, position);
		InstructionHandle handleForInsertion = instructionList
				.findHandle(position + offset);
		InstructionList newList = new InstructionList();
		for(String value : values) {
			if(value.startsWith("string:"))
				newList.append(factory.createConstant(value.substring("string:"
						.length())));
			else if(value.startsWith("int:"))
				newList.append(factory.createConstant(Integer.valueOf(value
						.substring("int:".length()))));
			else if(value.startsWith("long:"))
				newList.append(factory.createConstant(Long.valueOf(value
						.substring("long:".length()))));
			else if(value.startsWith("short:"))
				newList.append(factory.createConstant(Short.valueOf(value
						.substring("short:".length()))));
			else if(value.startsWith("double:"))
				newList.append(factory.createConstant(Double.valueOf(value
						.substring("double:".length()))));
			else if(value.startsWith("float:"))
				newList.append(factory.createConstant(Float.valueOf(value
						.substring("float:".length()))));
			else if(value.startsWith("byte:"))
				newList.append(factory.createConstant(Byte.valueOf(value
						.substring("byte:".length()))));
			else if(value.startsWith("char:"))
				newList.append(factory.createConstant((char) Integer
						.parseInt(value.substring("char:".length()))));
			else if(value.startsWith("bool:"))
				newList.append(factory.createConstant(Boolean.valueOf(value
						.substring("bool:".length()))));
			else if(value.startsWith("var:")) {
				String[] parts = value.split(":");
				String signature = parts[parts.length - 2];
				int index = Integer.parseInt(parts[parts.length - 1]);
				newList.append(InstructionFactory.createLoad(
						Type.getType(signature), index));
			}
		}
		Type returnType = Type.getReturnType(callbackMethodSignature);
		ObjectType callbackEventType = (ObjectType) Type
				.getType(CallbackEvent.class);
		Instruction invoke = factory.createInvoke(interfaceName,
				callbackMethodName, callbackEventType,
				Type.getArgumentTypes(callbackMethodSignature),
				Constants.INVOKESTATIC);
		newList.append(invoke);
		if(cancelTarget != -1) {
			int localVarCount = methodForCallback.isStatic() ? 0 : 1;
			localVarCount += methodForCallback.getArgumentTypes().length;
			for(Instruction i : instructionList.getInstructions())
				if(i instanceof StoreInstruction)
					localVarCount++;
			newList.append(new ASTORE(localVarCount));
			newList.append(new ALOAD(localVarCount));
			newList.append(factory.createInvoke(
					callbackEventType.getClassName(), "isCancelled",
					Type.BOOLEAN, new Type[0], Constants.INVOKEVIRTUAL));
			newList.append(new IFNE(instructionList.findHandle(cancelTarget
					+ getOffsetAtPosition(methodForCallback, cancelTarget))));
			if(callEventTarget != -1) {
				instructionList.insert(
						instructionList.findHandle(callEventTarget
								+ getOffsetAtPosition(methodForCallback,
										callEventTarget)), newList);
				updateMethodPositions(methodForCallback, callEventTarget,
						newList.getByteCode().length);
				newList = new InstructionList();
			}
			newList.append(new ALOAD(localVarCount));
		}

		if(!returnType.equals(Type.VOID)) {
			newList.append(factory.createInvoke(
					callbackEventType.getClassName(), "getReturnObject",
					Type.OBJECT, new Type[0], Constants.INVOKEVIRTUAL));
			if(!returnType.equals(Type.OBJECT))
				if(returnType instanceof BasicType) {
					Class<?> autoboxType = getAutoboxType((BasicType) returnType);
					newList.append(factory.createCheckCast((ObjectType) Type
							.getType(autoboxType)));
					newList.append(factory.createInvoke(autoboxType.getName(),
							Constants.TYPE_NAMES[returnType.getType()]
									+ "Value", returnType, new Type[0],
							Constants.INVOKEVIRTUAL));
				} else
					newList.append(factory
							.createCheckCast((ReferenceType) returnType));
			if(storeVar != -1)
				newList.append(InstructionFactory.createStore(returnType,
						storeVar));
		} else
			newList.append(new POP());

		updateMethodPositions(methodForCallback, position,
				newList.getByteCode().length);

		instructionList.insert(handleForInsertion, newList);
		instructionList.setPositions();
		methodForCallback.setInstructionList(instructionList);
		methodForCallback.setMaxLocals();
		methodForCallback.setMaxStack();
		classGen.replaceMethod(originalMethod, methodForCallback.getMethod());
	}

	private Class<?> getAutoboxType(BasicType type) {
		Class<?> autoboxType = null;
		switch(type.getType()) {
		case Constants.T_INT:
			autoboxType = Integer.class;
			break;
		case Constants.T_SHORT:
			autoboxType = Short.class;
			break;
		case Constants.T_LONG:
			autoboxType = Long.class;
			break;
		case Constants.T_BYTE:
			autoboxType = Byte.class;
			break;
		case Constants.T_DOUBLE:
			autoboxType = Double.class;
			break;
		case Constants.T_FLOAT:
			autoboxType = Float.class;
			break;
		case Constants.T_CHAR:
			autoboxType = Character.class;
			break;
		case Constants.T_BOOLEAN:
			autoboxType = Boolean.class;
			break;
		}
		return autoboxType;
	}
}