import org.darkstorm.minecraft.darkmod.events.PlayerProcessEvent;
import org.darkstorm.minecraft.darkmod.hooks.client.Player;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.tools.events.*;
import org.lwjgl.input.Keyboard;

public class SprintMod extends Mod implements EventListener {

	@Override
	public String getShortDescription() {
		return "Automagically sprint ALL THE TIME!";
	}

	@Override
	public String getFullDescription() {
		return "";
	}

	@Override
	public String getName() {
		return "Sprint Mod";
	}

	@Override
	public ModControl getControlOption() {
		return ModControl.TOGGLE;
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public void onStart() {
		eventManager.addListener(PlayerProcessEvent.class, this);
	}

	@Override
	public void onStop() {
		eventManager.removeListener(PlayerProcessEvent.class, this);
	}

	@Override
	public int loop() {
		return 1000;
	}

	@Override
	public void onEvent(Event event) {
		Player player = ((PlayerProcessEvent) event).getPlayer();
		if(player.equals(minecraft.getPlayer()) && player.getFoodStatus().getFoodLevel() > 6) {
			if(Keyboard.isKeyDown(Keyboard.KEY_W) && !Keyboard.isKeyDown(Keyboard.KEY_S))
				player.setSprinting(true);
			else
				player.setSprinting(false);
		}
	}
}
