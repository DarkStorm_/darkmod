import java.util.*;

import java.lang.reflect.Field;

import org.darkstorm.minecraft.darkmod.DarkMod;
import org.darkstorm.minecraft.darkmod.access.injection.InjectionHandler;
import org.darkstorm.minecraft.darkmod.events.*;
import org.darkstorm.minecraft.darkmod.hooks.client.Packet;
import org.darkstorm.minecraft.darkmod.hooks.client.packets.Packet3Chat;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.minecraft.darkmod.mod.util.constants.ChatColor;
import org.darkstorm.tools.events.*;
import org.darkstorm.tools.events.EventListener;

public class ReiMinimapAllowAllMod extends Mod implements EventListener {
	@Override
	public void onStart() {
		eventManager.addListener(PacketEvent.class, this);
	}

	@Override
	public void onStop() {
		eventManager.removeListener(PacketEvent.class, this);
	}

	@Override
	public ModControl getControlOption() {
		return ModControl.TOGGLE;
	}

	@Override
	public String getName() {
		return "ReiMinimap AllowAll Mod";
	}

	@Override
	public String getShortDescription() {
		return "Enables cave rendering and all entities radar";
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public int loop() throws InterruptedException {
		try {
			Class<?> minimapClass = Class.forName("reifnsk.minimap.ReiMinimap",
					true, ((InjectionHandler) DarkMod.getInstance()
							.getAccessHandler()).getClassLoader());
			Field instanceField = minimapClass.getDeclaredField("instance");
			Object minimap = instanceField.get(null);

			List<Field> allowFields = new ArrayList<Field>();
			List<Field> visibleFields = new ArrayList<Field>();
			List<Field> configFields = new ArrayList<Field>();

			Field allowCavemapField = minimapClass
					.getDeclaredField("allowCavemap");
			if(!allowCavemapField.isAccessible())
				allowCavemapField.setAccessible(true);
			allowCavemapField.setBoolean(minimap, true);

			for(Field field : minimapClass.getDeclaredFields()) {
				if(!field.getType().equals(Boolean.TYPE))
					continue;
				String fieldName = field.getName();
				if(fieldName.startsWith("allowEntit"))
					allowFields.add(field);
				else if(fieldName.startsWith("visibleEntit"))
					visibleFields.add(field);
				else if(fieldName.startsWith("configEntit")
						&& !fieldName.equals("configEntityLightning")
						&& !fieldName.equals("configEntityDirection"))
					configFields.add(field);
			}
			if(allowFields.size() != visibleFields.size()
					|| allowFields.size() != configFields.size())
				throw new RuntimeException(
						"inequal allow, visible, and config fields");

			boolean[] allows = new boolean[allowFields.size()];
			boolean[] configs = new boolean[configFields.size()];

			for(int i = 0; i < allows.length; i++) {
				Field field = allowFields.get(i);
				if(!field.isAccessible())
					field.setAccessible(true);
				allows[i] = field.getBoolean(minimap);
			}

			for(int i = 0; i < configs.length; i++) {
				Field field = configFields.get(i);
				if(!field.isAccessible())
					field.setAccessible(true);
				configs[i] = field.getBoolean(minimap);
			}

			boolean valueChanged = false;
			for(int i = 0; i < allows.length; i++) {
				boolean allow = allows[i];
				if(!allow) {
					Field allowField = allowFields.get(i);
					allowField.setBoolean(minimap, true);
					Field visibleField = visibleFields.get(i);
					visibleField.setAccessible(true);
					visibleField.set(minimap, configs[i]);
					valueChanged = true;
				}
			}
			if(valueChanged)
				displayText(ChatColor.AQUA
						+ "Successfully changed minimap values!");
		} catch(ClassNotFoundException exception) {
			exception.printStackTrace();
			displayText(ChatColor.RED + "Minimap not found!");
			handler.stopMod(getName());
		} catch(Throwable exception) {
			exception.printStackTrace();
		}
		return 5000;
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof PacketReceivedEvent) {
			Packet packet = ((PacketEvent) event).getPacket();
			if(packet instanceof Packet3Chat) {
				Packet3Chat chatPacket = (Packet3Chat) packet;
				String message = chatPacket.getMessage();
				message = message.replace(" ", "");
				message = ChatColor.removeColors(message);
				if(message.isEmpty()) {
					System.out.println(chatPacket.getMessage());
					chatPacket.setMessage("");
				}
			}
		}
	}
}
