package org.darkstorm.minecraft.darkmod.events;

import org.darkstorm.minecraft.darkmod.hooks.client.Packet;
import org.darkstorm.tools.events.Event;

public abstract class PacketEvent extends Event {

	public PacketEvent(Packet packet) {
		super(packet);
	}

	public Packet getPacket() {
		return (Packet) info[0];
	}

}
