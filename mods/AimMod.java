import java.util.List;

import org.darkstorm.minecraft.darkmod.events.RenderEvent;
import org.darkstorm.minecraft.darkmod.hooks.client.*;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.tools.events.Event;

public class AimMod extends Mod {
	private AutoAttackMod attackMod;
	private Animable target;

	public AimMod() {
	}

	@Override
	public void onStart() {
		attackMod = (AutoAttackMod) handler.getModByName("Auto Attack Mod");
		eventManager.addListener(RenderEvent.class, this);
	}

	@Override
	public void onStop() {
		eventManager.removeListener(RenderEvent.class, this);
	}

	@Override
	public int loop() throws InterruptedException {
		return 90000;
	}

	@Override
	public String getName() {
		return "Aim Mod";
	}

	@Override
	public String getShortDescription() {
		return "Aim at people who are closeby. Use bow mod for far away";
	}

	@Override
	public ModControl getControlOption() {
		return ModControl.TOGGLE;
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof RenderEvent) {
			Player player = minecraft.getPlayer();
			if(player == null) {
				target = null;
				return;
			}
			target = getTarget();
			if(target == null)
				return;
			if(player.getItemInUse() != null && player.getItemInUse().getID() == 261) {
				int var6 = player.getItemInUse().getMaxItemUseTime() - player.getItemInUseTimer();
				float var7 = var6 / 20.0F;
				var7 = (var7 * var7 + var7 * 2.0F) / 3.0F;
				if(var7 >= 1.0F)
					var7 = 1.0F;

				double v = var7 * 3.0F;
				double g = 0.05F;

				setAngles(target, v, g);
			} else {
				if(getDistanceBetween(player, target) > 6) {
					target = null;
					return;
				}
				World world = minecraft.getWorld();
				if(player == null || world == null)
					return;
				player.setRotationX(getRotationXForShooting(target));
				player.setRotationY(getRotationYForShooting(target));
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Entity getClosest(double maxDistance) {
		Player player = minecraft.getPlayer();
		World world = minecraft.getWorld();
		if(world == null || player == null)
			return null;
		Entity closestEntity = null;
		double closestDistance = 999;
		for(Entity entity : (List<Entity>) world.getEntities()) {
			if(entity.getID() == player.getID() || !(entity instanceof Humanoid))
				continue;
			if(entity instanceof Humanoid) {
				Humanoid otherPlayer = (Humanoid) entity;
				if(attackMod.isFriend(otherPlayer.getName()) || (attackMod.getTeam() != null && attackMod.getTeam().isOnTeam(otherPlayer)))
					continue;
			}
			double distance = getDistanceBetween(player, entity);
			if((maxDistance <= 0 || distance <= maxDistance) && distance < closestDistance) {
				closestEntity = entity;
				closestDistance = distance;
			}
		}
		return closestEntity;
	}

	public float getRotationXForShooting(Entity entity) {
		Player player = minecraft.getPlayer();
		double d = entity.getX() - player.getX();
		double d1 = entity.getZ() - player.getZ();
		return (float) ((Math.atan2(d1, d) * 180D) / 3.1415927410125732D) - 90F;
	}

	public float getRotationYForShooting(Entity entity) {
		Player player = minecraft.getPlayer();
		double dis1 = entity.getY() + 1 - player.getY() + 1;
		double dis2 = Math.sqrt(Math.pow(entity.getX() - player.getX(), 2) + Math.pow(entity.getZ() - player.getZ(), 2));
		return (float) ((Math.atan2(dis2, dis1) * 180D) / 3.1415927410125732D) - 80F - ((float) Math.pow(getDistanceBetween(player, entity) / 5, 2));
	}

	public float getDifference(float f1, float f2) {
		if(f1 == f2)
			return 0;
		if(f1 < f2)
			return Math.abs(f2 - f1);
		return Math.abs(f1 - f2);
	}

	private Animable getTarget() {
		return getClosestEntityToCursor(60);
	}

	/**
	 * Return the closest entity based on angles.
	 * 
	 * @param angle
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Animable getClosestEntityToCursor(final float angle) {
		float distance = angle; // localized distance variable
		Animable tempEntity = null;

		for(Entity entity : (List<Entity>) minecraft.getWorld().getEntities()) {
			if(!(entity instanceof Animable) || !canAttack((Animable) entity))
				continue;
			if(entity instanceof Humanoid) {
				if(entity.getID() == minecraft.getPlayer().getID())
					continue;
				Humanoid otherPlayer = (Humanoid) entity;
				if(attackMod.isFriend(otherPlayer.getName()) || (attackMod.getTeam() != null && attackMod.getTeam().isOnTeam(otherPlayer)))
					continue;
			}
			Animable living = (Animable) entity;

			final float[] angles = getAngles(living);
			final float curDistance = getDistanceBetweenAngles(minecraft.getPlayer().getRotationX(), angles[0]) + getDistanceBetweenAngles(minecraft.getPlayer().getRotationY(), angles[1]) / 2;

			if(curDistance <= distance) {
				distance = curDistance;
				tempEntity = living;
			}
		}

		return tempEntity;
	}

	private boolean canAttack(Animable entity) {
		return entity instanceof Humanoid;
	}

	/**
	 * Return an array of necessary angles to attack the desired entity.
	 * 
	 * @author Stul Missouri
	 * @see com.stlmissouri.cobalt.module.combat.KillAura
	 * @param entityLiving
	 *            The desired entity
	 * @return Float array of angles
	 */
	public float[] getAngles(final Animable entityLiving) {
		Player player = minecraft.getPlayer();
		double difX = entityLiving.getX() - player.getX(), difY = (entityLiving.getY() - player.getY()) + (1.62000000476837 / 1.4F), difZ = entityLiving.getZ() - player.getZ();
		double helper = Math.sqrt(difX * difX + difZ * difZ);

		float yaw = (float) (Math.atan2(difZ, difX) * 180 / Math.PI) - 90;
		float pitch = (float) -(Math.atan2(difY, helper) * 180 / Math.PI);
		return(new float[] { yaw, pitch });
	}

	/**
	 * Returns the fixed distance between angles.
	 * 
	 * @param par1
	 * @param par2
	 * @return
	 */
	public float getDistanceBetweenAngles(final float par1, final float par2) {
		return (Math.abs((par1 % 360) - (par2 % 360))) % 360;
	}

	// Minimized so you can't see my l33t hax
	private double theta(double v, double g, double x, double y) {
		double yv = 2 * y * (v * v);
		double gx = g * (x * x);
		double g2 = g * (gx + yv);
		double insqrt = (v * v * v * v) - g2;
		double sqrt = Math.sqrt(insqrt);

		double numerator = (v * v) + sqrt;
		double numerator2 = (v * v) - sqrt;

		double atan1 = Math.atan2(numerator, g * x);
		double atan2 = Math.atan2(numerator2, g * x);

		return Math.min(atan1, atan2);
	}

	private double time(double v, double g, float launchAngle) {
		double v1 = v * (Math.sin(launchAngle));
		v1 += v1;
		return v1 / g;
	}

	private float getLaunchAngle(Animable targetEntity, double v, double g) {
		Player player = minecraft.getPlayer();
		double yDif = ((targetEntity.getY()) - (player.getY() - 1.62000000476837));
		double xDif = (targetEntity.getX() - player.getX());
		double zDif = (targetEntity.getZ() - player.getZ());
		double xCoord = Math.sqrt((xDif * xDif) + (zDif * zDif));

		float angle = (float) theta(v, g, xCoord, yDif);
		return angle;
	}

	private void setAngles(Animable entityLiving, double v, double g) {
		Player player = minecraft.getPlayer();
		double deg = -Math.toDegrees(getLaunchAngle(entityLiving, v, g));
		if(Double.isNaN(deg))
			return;
		player.setRotationY((float) deg);
		double difX = entityLiving.getX() - player.getX(), difZ = entityLiving.getZ() - player.getZ();
		float yaw = (float) (Math.atan2(difZ, difX) * 180 / Math.PI) - 90;
		player.setRotationX(yaw);
	}
}
