package org.darkstorm.minecraft.darkmod.mod;

import java.util.ArrayList;

import org.darkstorm.minecraft.darkmod.DarkMod;
import org.darkstorm.minecraft.darkmod.access.AccessHandler;
import org.darkstorm.minecraft.darkmod.events.*;
import org.darkstorm.minecraft.darkmod.hooks.client.*;
import org.darkstorm.minecraft.darkmod.hooks.client.packets.Packet3Chat;
import org.darkstorm.minecraft.darkmod.mod.Mod.ModControl;
import org.darkstorm.minecraft.darkmod.mod.commands.CommandManager;
import org.darkstorm.minecraft.darkmod.ui.ModHandlerUI;
import org.darkstorm.tools.events.*;
import org.darkstorm.tools.loopsystem.LoopManager;

public class ModHandler implements EventListener {
	private AccessHandler accessHandler;
	private EventManager eventManager;
	private LoopManager loopManager;
	private ModLoader modLoader;
	private ModMenuHandler menuHandler;
	private CommandManager commandManager;
	private ModHandlerUI ui;
	private ArrayList<Mod> mods;
	private Object modsLock = new Object();

	public ModHandler(AccessHandler accessHandler) {
		DarkMod darkMod = DarkMod.getInstance();
		this.accessHandler = accessHandler;
		eventManager = darkMod.getEventManager();
		loopManager = new LoopManager(new ThreadGroup("Mod Threads"));
		loopManager.stopAll();
		mods = new ArrayList<Mod>();
		modLoader = new ModLoader(this);
		ui = new ModHandlerUI(this);
		commandManager = new CommandManager(this);
		menuHandler = new ModMenuHandler(this);
		modLoader.reloadMods();
		eventManager.addListener(ShutdownEvent.class, this);
		eventManager.addListener(CallbackEvent.class, this);
	}

	public void addMod(Mod mod) {
		synchronized(modsLock) {
			if(mods.contains(mod))
				throw new IllegalArgumentException("Mod already loaded!");
			if(mod.getControlOption() == null)
				throw new IllegalArgumentException("Mod doesn\'t specify control");
			try {
				mod.init(this);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
			mods.add(mod);
			menuHandler.updateMod(mod);
			System.out.println("Mod added: " + mod.getName());
			if(mod.getControlOption() == ModControl.NONE)
				mod.start();
		}
	}

	public void removeMod(Mod mod) {
		synchronized(modsLock) {
			if(!mods.contains(mod))
				throw new IllegalArgumentException("Mod not loaded!");
			if(mod.isRunning())
				mod.stop();
			mods.remove(mod);
			menuHandler.removeMod(mod);
		}
	}

	public void clearMods() {
		synchronized(modsLock) {
			Mod[] mods = this.mods.toArray(new Mod[this.mods.size()]);
			for(Mod mod : mods)
				removeMod(mod);
		}
	}

	public Mod[] getMods() {
		synchronized(modsLock) {
			return mods.toArray(new Mod[mods.size()]);
		}
	}

	public Mod startMod(String name) {
		Mod mod = getModByName(name);
		if(mod != null && !mod.isRunning())
			mod.start();
		return mod;
	}

	public Mod stopMod(String name) {
		Mod mod = getModByName(name);
		if(mod != null && mod.isRunning())
			mod.stop();
		return mod;
	}

	void updateModMenu(Mod mod) {
		menuHandler.updateMod(mod);
	}

	public Mod getModByName(String name) {
		synchronized(modsLock) {
			for(Mod mod : mods)
				if(name.equals(mod.getName()))
					return mod;
		}
		return null;
	}

	public void reloadMods() {
		System.out.println("Reloading mod...");
		modLoader.reloadMods();
		System.out.println("Loaded!");
	}

	public ModHandlerUI getUI() {
		return ui;
	}

	public void showUI() {
		ui.setVisible(true);
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof ShutdownEvent) {
			synchronized(modsLock) {
				for(Mod mod : mods)
					if(mod.isRunning())
						mod.stop();
			}
		} else if(event instanceof CallbackEvent && ((CallbackEvent) event).getCallback().equals("onCrash")) {
			// CrashReport report = (CrashReport) ((CallbackEvent) event)
			// .getArguments()[0];
			// CrashReportInfo info = report.getInfo();
			// StringBuilder out = new StringBuilder();
			// info.output(out);
			// System.out.println(out.toString());
		}
		if(event instanceof CallbackEvent) {
			CallbackEvent callback = (CallbackEvent) event;
			EventManager manager = DarkMod.getInstance().getModHandler().getEventManager();
			if(callback.getCallback().equals("render")) {
				String value = (String) callback.getArguments()[0];
				int status = RenderEvent.UNKNOWN;
				if(value.equals("pre"))
					status = RenderEvent.PRE_RENDER;
				else if(value.equals("render"))
					status = RenderEvent.RENDER;
				else if(value.equals("game"))
					status = RenderEvent.GAME_RENDER;
				else if(value.equals("post"))
					status = RenderEvent.POST_RENDER;
				else if(value.equals("entities_prepare"))
					status = RenderEvent.RENDER_ENTITIES_PREPARE;
				else if(value.equals("entities_global"))
					status = RenderEvent.RENDER_ENTITIES_GLOBAL;
				else if(value.equals("entities_entities"))
					status = RenderEvent.RENDER_ENTITIES;
				else if(value.equals("entities_tileentities"))
					status = RenderEvent.RENDER_ENTITIES_TILE_ENTITIES;
				else if(value.equals("entities_end"))
					status = RenderEvent.RENDER_ENTITIES_END;
				manager.sendEvent(new RenderEvent(status));
			} else if(callback.getCallback().equals("onTick")) {
				manager.sendEvent(new TickEvent());
			} else if(callback.getCallback().equals("onPlayerProcess")) {
				Player player = (Player) callback.getArguments()[0];
				String value = (String) callback.getArguments()[1];
				int status = -1;
				if(value.equals("pre_super"))
					status = PlayerProcessEvent.PRE_SUPERCLASS_PROCESS;
				else if(value.equals("pre"))
					status = PlayerProcessEvent.START;
				else if(value.equals("post"))
					status = PlayerProcessEvent.FINISH;
				manager.sendEvent(new PlayerProcessEvent(player, status));
			} else if(callback.getCallback().equals("onPacketReceived")) {
				Packet packet = (Packet) callback.getArguments()[0];
				manager.sendEvent(new PacketReceivedEvent(packet));
				if(packet instanceof Packet3Chat)
					manager.sendEvent(new ChatEvent(ChatEvent.DISPLAYED, ((Packet3Chat) packet).getMessage()));
			} else if(callback.getCallback().equals("onPacketSent")) {
				PacketSentEvent packetEvent = new PacketSentEvent((Packet) callback.getArguments()[0]);
				manager.sendEvent(packetEvent);
				callback.setCancelled(packetEvent.isCancelled());
			} else if(callback.getCallback().equals("handleText")) {
				manager.sendEvent(new ChatEvent(ChatEvent.SENT, (String) callback.getArguments()[0]));
			}
		}
		// if(event instanceof CallbackEvent) {
		// CallbackEvent callback = (CallbackEvent) event;
		// String args = "";
		// if(callback.getArguments().length > 0) {
		// args = ": " + callback.getArguments()[0].toString();
		// for(int i = 1; i < callback.getArguments().length; i++)
		// args += ", " + callback.getArguments()[i];
		// }
		// System.out.println(((CallbackEvent) event).getCallback() + args);
		// }
	}

	public AccessHandler getAccessHandler() {
		return accessHandler;
	}

	public CommandManager getCommandManager() {
		return commandManager;
	}

	public EventManager getEventManager() {
		return eventManager;
	}

	public LoopManager getLoopManager() {
		return loopManager;
	}
}
