import org.darkstorm.minecraft.darkmod.hooks.client.*;
import org.darkstorm.minecraft.darkmod.hooks.client.packets.*;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.minecraft.darkmod.mod.commands.*;
import org.darkstorm.minecraft.darkmod.mod.util.constants.ChatColor;
import org.darkstorm.minecraft.darkmod.tools.*;
import org.darkstorm.tools.strings.StringTools;

public class MassiveEatMod extends Mod implements CommandListener {

	private int foodID = 320;
	private long lastRefill = 0;

	public MassiveEatMod() {
	}

	@Override
	public void onStart() {
		commandManager.registerListener(new Command("meat", "/meat id <id>",
				"Eat EXTREME!"), this);
	}

	@Override
	public void onStop() {
		commandManager.unregisterListener("meat");
	}

	@Override
	public ModControl getControlOption() {
		return ModControl.TOGGLE;
	}

	@Override
	public String getName() {
		return "MassiveEating Mod";
	}

	@Override
	public String getShortDescription() {
		return "Eats at an extreme rate";
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public int loop() throws InterruptedException {
		if(minecraft.getWorld() == null
				|| !(minecraft.getWorld() instanceof MultiplayerWorld))
			return 500;
		MultiplayerWorld world = minecraft.getWorld();
		NetworkHandler networkHandler = world.getNetworkHandler();
		MultiplayerPlayer player = minecraft.getPlayer();
		if(player == null)
			return 500;
		Inventory inventory = player.getInventory();
		if(System.currentTimeMillis() - lastRefill > 1500) {
			int count = 0;
			for(int i = 0; i < 9; i++) {
				InventoryItem item = inventory.getItemAt(i);
				if(item != null && item.getID() == foodID)
					count += item.getStackCount();
			}
			if(count < 256) {
				player.sendText("/kit pvp");
				lastRefill = System.currentTimeMillis();
			}
		}
		for(int i = 0; i < 9; i++) {
			InventoryItem item = inventory.getItemAt(i);
			if(item != null) {
				int id = item.getID();
				if(id == foodID) {
					eatFood(networkHandler, i, item);
					Thread.sleep(50);
				}
			}
		}
		return 50;
	}

	private void eatFood(NetworkHandler networkHandler, int index,
			InventoryItem item) {
		Player player = minecraft.getPlayer();
		Inventory inventory = player.getInventory();
		int currentIndex = inventory.getSelectedIndex();
		Packet16BlockItemSwitch selectPacket = (Packet16BlockItemSwitch) ReflectionUtil
				.instantiate(ClassRepository
						.getClassForInterface(Packet16BlockItemSwitch.class));
		selectPacket.setID(index);
		networkHandler.sendPacket(selectPacket);
		Packet15BlockPlace placePacket = (Packet15BlockPlace) ReflectionUtil
				.instantiate(ClassRepository
						.getClassForInterface(Packet15BlockPlace.class));
		placePacket.setX(-1);
		placePacket.setY(-1);
		placePacket.setZ(-1);
		placePacket.setDirection(-1);
		placePacket.setItem(item);
		networkHandler.sendPacket(placePacket);
		selectPacket = (Packet16BlockItemSwitch) ReflectionUtil
				.instantiate(ClassRepository
						.getClassForInterface(Packet16BlockItemSwitch.class));
		selectPacket.setID(currentIndex);
		networkHandler.sendPacket(selectPacket);
	}

	@Override
	public void onCommand(String command) {
		String[] parts = command.split(" ");
		if(parts[0].equalsIgnoreCase("meat")) {
			if(parts.length != 3 || !StringTools.isInteger(parts[2])) {
				displayText(ChatColor.GRAY + "Use /meat id <id>");
				return;
			}
			if(parts[1].equalsIgnoreCase("id")) {
				foodID = Integer.parseInt(parts[2]);
				displayText(ChatColor.GRAY + "ID of food to eat set to: "
						+ ChatColor.GOLD + foodID);
			}
		}
	}
}
