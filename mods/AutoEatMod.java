import org.darkstorm.minecraft.darkmod.events.*;
import org.darkstorm.minecraft.darkmod.hooks.client.*;
import org.darkstorm.minecraft.darkmod.hooks.client.packets.*;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.minecraft.darkmod.mod.commands.*;
import org.darkstorm.minecraft.darkmod.mod.util.constants.ChatColor;
import org.darkstorm.minecraft.darkmod.tools.*;
import org.darkstorm.tools.events.Event;
import org.darkstorm.tools.strings.StringTools;

public class AutoEatMod extends Mod implements CommandListener {
	private enum EatMode {
		NORMAL,
		NOCHEAT
	}

	private int health = 8, foodID = 320, minFoodToRefill = 4;
	private EatMode eatMode = EatMode.NORMAL;
	private String refillText = null;
	private long lastRefill = 0;

	private int invalidCount = 4;
	private short lastInventoryAction = 0;
	private boolean lastActionSlotInvalid = false;
	private int timer = 0;

	public AutoEatMod() {
	}

	@Override
	public void onStart() {
		commandManager.registerListener(new Command("eat",
				"/eat <hp <health> | id <food id> | mode <normal|instant>>",
				"Use /eat hp <health> to set health, /eat id to set food id,"
						+ " /eat mode to set the mode of eating food"), this);
		eventManager.addListener(TickEvent.class, this);
	}

	@Override
	public void onStop() {
		eventManager.removeListener(TickEvent.class, this);
		commandManager.unregisterListener("eat");
	}

	@Override
	public ModControl getControlOption() {
		return ModControl.TOGGLE;
	}

	@Override
	public String getName() {
		return "AutoEating Mod";
	}

	@Override
	public String getShortDescription() {
		return "Automatically eats when your health gets low";
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public int loop() throws InterruptedException {
		if(eatMode != EatMode.NOCHEAT || minecraft.getWorld() == null
				|| !(minecraft.getWorld() instanceof MultiplayerWorld))
			return 500;
		MultiplayerWorld world = minecraft.getWorld();
		NetworkHandler networkHandler = world.getNetworkHandler();
		MultiplayerPlayer player = minecraft.getPlayer();
		if(player == null)
			return 500;
		int health = player.getHealth();
		if(health <= 20 - this.health) {
			Inventory inventory = player.getInventory();
			if(refillText != null
					&& System.currentTimeMillis() - lastRefill > 3000) {
				int count = 0;
				for(int i = 0; i < 36; i++) {
					InventoryItem item = inventory.getItemAt(i);
					if(item != null && item.getID() == foodID)
						count += item.getStackCount();
				}
				if(count < minFoodToRefill) {
					player.sendText(refillText);
					lastRefill = System.currentTimeMillis();
				}
			}
			for(int i = 0; i < 36; i++) {
				InventoryItem item = inventory.getItemAt(i);
				if(item != null) {
					int id = item.getID();
					if(id == foodID) {
						if(i > 8) {
							int slotIndex = inventory.getIndexOfEmptySlot();
							if(slotIndex < 0 || slotIndex > 8)
								slotIndex = 8;
							boolean switchSpaces = slotIndex == -1;
							if(switchSpaces)
								slotIndex = 8;
							moveInventory(i, slotIndex, switchSpaces);
							eatFood(networkHandler, slotIndex,
									inventory.getItemAt(slotIndex));
							break;
						}
						eatFood(networkHandler, i, item);
						break;
					}
				}
			}
		}
		return 10;
	}

	private void moveInventory(int slot1, int slot2, boolean switchItems) {
		Player player = minecraft.getPlayer();
		Inventory inventory = player.getInventory();
		MultiplayerWorld world = minecraft.getWorld();
		NetworkHandler networkHandler = world.getNetworkHandler();
		lastInventoryAction += 1;
		InventoryItem slot1Item = inventory.getItemAt(slot1);
		Packet102WindowClick inventoryPacket = ReflectionUtil
				.instantiate(ClassRepository
						.getClassForInterface(Packet102WindowClick.class));
		inventoryPacket.setAction(lastInventoryAction);
		inventoryPacket.setButton(0);
		inventoryPacket.setHoldingShift(0);
		inventoryPacket.setSlot(calculateSendIndex(slot1));
		inventoryPacket.setWindowID(0);
		inventoryPacket.setItem(slot1Item);
		networkHandler.sendPacket(inventoryPacket);
		inventory.setItemAt(slot1, null);
		lastInventoryAction += 1;
		InventoryItem slot2Item = inventory.getItemAt(slot2);
		inventoryPacket = ReflectionUtil.instantiate(ClassRepository
				.getClassForInterface(Packet102WindowClick.class));
		inventoryPacket.setAction(lastInventoryAction);
		inventoryPacket.setButton(0);
		inventoryPacket.setHoldingShift(0);
		inventoryPacket.setSlot(calculateSendIndex(slot2));
		inventoryPacket.setWindowID(0);
		inventoryPacket.setItem(slot2Item);
		networkHandler.sendPacket(inventoryPacket);
		inventory.setItemAt(slot2, slot1Item);
		if(!switchItems)
			return;
		lastInventoryAction += 1;
		inventoryPacket = ReflectionUtil.instantiate(ClassRepository
				.getClassForInterface(Packet102WindowClick.class));
		inventoryPacket.setAction(lastInventoryAction);
		inventoryPacket.setButton(0);
		inventoryPacket.setHoldingShift(0);
		inventoryPacket.setSlot(calculateSendIndex(slot1));
		inventoryPacket.setWindowID(0);
		inventoryPacket.setItem(null);
		networkHandler.sendPacket(inventoryPacket);
		inventory.setItemAt(slot1, slot2Item);
	}

	private int calculateSendIndex(int index) {
		if(index >= 0 && index < 9)
			return index + 36;
		return index;
	}

	private void eatFood(NetworkHandler networkHandler, int index,
			InventoryItem item) {
		Player player = minecraft.getPlayer();
		Inventory inventory = player.getInventory();
		int currentIndex = inventory.getSelectedIndex();
		Packet16BlockItemSwitch selectPacket = ReflectionUtil
				.instantiate(ClassRepository
						.getClassForInterface(Packet16BlockItemSwitch.class));
		selectPacket.setID(index);
		networkHandler.sendPacket(selectPacket);
		Packet15BlockPlace placePacket = ReflectionUtil
				.instantiate(ClassRepository
						.getClassForInterface(Packet15BlockPlace.class));
		placePacket.setX(-1);
		placePacket.setY(-1);
		placePacket.setZ(-1);
		placePacket.setDirection(-1);
		placePacket.setItem(item);
		networkHandler.sendPacket(placePacket);
		selectPacket = ReflectionUtil.instantiate(ClassRepository
				.getClassForInterface(Packet16BlockItemSwitch.class));
		selectPacket.setID(currentIndex);
		networkHandler.sendPacket(selectPacket);
		player.setHealth(player.getHealth() + health);
		if(item.getStackCount() > 1)
			item.setStackCount(item.getStackCount() - 1);
		else
			inventory.setItemAt(index, null);
	}

	@Override
	public void onCommand(String command) {
		String[] parts = command.split(" ");
		if(parts[0].equalsIgnoreCase("eat")) {
			if(parts[1].equalsIgnoreCase("mode")) {
				eatMode = EatMode.valueOf(parts[2].toUpperCase());
				displayText(ChatColor.GRAY
						+ "Mode of eating set to: "
						+ ChatColor.GOLD
						+ (eatMode != null ? eatMode.name().toLowerCase()
								: "none"));
			} else if(!StringTools.isInteger(parts[2])) {
				displayText(ChatColor.GRAY
						+ "Use /eat <hp <health> | id <food id> | mode <normal|instant>>");
				return;
			}
			if(parts[1].equalsIgnoreCase("hp")) {
				health = Integer.parseInt(parts[2]);
				displayText(ChatColor.GRAY + "Health to eat at set to: "
						+ ChatColor.GOLD + health);
			} else if(parts[1].equalsIgnoreCase("id")) {
				foodID = Integer.parseInt(parts[2]);
				displayText(ChatColor.GRAY + "ID of food to eat set to: "
						+ ChatColor.GOLD + foodID);
			} else if(parts[1].equals("refill")) {
				if(parts[2].equalsIgnoreCase("text")) {
					if(parts.length > 1) {
						String text = parts[1];
						if(parts.length > 2)
							for(int i = 2; i < parts.length; i++)
								text += " " + parts[i];
						refillText = text;
						displayText(ChatColor.GRAY + "Refill text set to: "
								+ ChatColor.GOLD + text);
					} else {
						refillText = null;
						displayText(ChatColor.GRAY + "Refill text reset");
					}
				} else if(parts[2].equalsIgnoreCase("amount")) {
					minFoodToRefill = Integer.parseInt(parts[2]);
					displayText(ChatColor.GRAY
							+ "Minimum food to refill set to: "
							+ ChatColor.GOLD + minFoodToRefill);
				}
			} else
				displayText(ChatColor.GRAY
						+ "Use /eat <hp <health> | id <food id> | mode <normal|instant>>");
		}
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof TickEvent && eatMode == EatMode.NORMAL) {
			if(timer > 0) {
				timer--;
				return;
			} else
				timer = 4;
			if(minecraft.getWorld() == null
					|| !(minecraft.getWorld() instanceof MultiplayerWorld))
				return;
			MultiplayerPlayer player = minecraft.getPlayer();
			if(player == null)
				return;
			int health = player.getHealth();
			if(health <= 20 - this.health) {
				Inventory inventory = player.getInventory();
				if(refillText != null
						&& System.currentTimeMillis() - lastRefill > 3000) {
					int count = 0;
					for(int i = 0; i < 36; i++) {
						InventoryItem item = inventory.getItemAt(i);
						if(item != null && item.getID() == foodID)
							count += item.getStackCount();
					}
					if(count < minFoodToRefill) {
						player.sendText(refillText);
						lastRefill = System.currentTimeMillis();
					}
				}
				eatItem();
			}
		} else if(event instanceof PacketEvent) {
			PacketEvent packetEvent = (PacketEvent) event;
			if(!(packetEvent.getPacket() instanceof Packet102WindowClick))
				return;
			Packet102WindowClick packet = (Packet102WindowClick) packetEvent
					.getPacket();
			int lastActionSlot = packet.getSlot();
			if(lastActionSlotInvalid) {
				packet.setSlot(5);
				lastInventoryAction += 1;
				packet.setAction(lastInventoryAction);
				lastActionSlotInvalid = false;
				return;
			}
			if(lastActionSlot > 50 && invalidCount < 4) {
				packet.setSlot(5);
				lastInventoryAction += 1;
				packet.setAction(lastInventoryAction);
				lastActionSlotInvalid = true;
				invalidCount++;
				return;
			}
			lastInventoryAction = packet.getAction();
		}
	}

	private void eatItem() {
		Player player = minecraft.getPlayer();
		Inventory inventory = player.getInventory();
		NetworkHandler handler = minecraft.getWorld().getNetworkHandler();
		try {
			int currentSlot = -1;
			InventoryItem currentItem = null;
			for(int j1 = 0; j1 <= 36; j1++) {
				InventoryItem is = inventory.getItemAt(j1);
				if(is != null && is.getID() == foodID) {
					currentSlot = j1;
					currentItem = is;
					break;
				}
			}
			if(currentItem == null)
				return;
			PlayerController controller = minecraft.getPlayerController();
			// controller.windowClick(0, 37, 0, 0, player);
			int selectedIndex;
			if(currentSlot > 8) {
				controller.windowClick(0, currentSlot, 0, 1, player);
				selectedIndex = 1;
			} else
				selectedIndex = currentSlot;
			// controller.windowClick(0, 35, 0, 1, player);
			if(controller.getSelectedItemIndex() != selectedIndex) {
				inventory.setSelectedIndex(selectedIndex);
				controller.setSelectedItemIndex(selectedIndex);
				Packet16BlockItemSwitch switchPacket = ReflectionUtil
						.instantiate(ClassRepository
								.getClassForInterface(Packet16BlockItemSwitch.class));
				switchPacket.setID(selectedIndex);
				handler.sendPacket(switchPacket);
			}
			Packet15BlockPlace placePacket = ReflectionUtil
					.instantiate(ClassRepository
							.getClassForInterface(Packet15BlockPlace.class));
			placePacket.setX(-1);
			placePacket.setY(-1);
			placePacket.setZ(-1);
			placePacket.setDirection(-1);
			placePacket.setItem(currentItem);
			handler.sendPacket(placePacket);
			// Packet14BlockDig digPacket = ReflectionUtil
			// .instantiate(ClassRepository
			// .getClassForInterface(Packet14BlockDig.class));
			// digPacket.setStatus(4);
			// digPacket.setFace(0);
			// digPacket.setX(0);
			// digPacket.setY(0);
			// digPacket.setZ(0);
			// handler.sendPacket(digPacket);
		} catch(Exception e) {}
	}

}
