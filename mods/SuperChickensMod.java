import java.util.List;

import org.darkstorm.minecraft.darkmod.events.TickEvent;
import org.darkstorm.minecraft.darkmod.hooks.client.*;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.tools.events.Event;

public class SuperChickensMod extends Mod {
	@Override
	public String getName() {
		return "Super Chickens Mod";
	}

	@Override
	public String getShortDescription() {
		return "EGGS!!! What did you put in their fertilizer!?";
	}

	@Override
	public String getFullDescription() {
		return "<html>Makes chickens lay eggs at 1 per frame (causes laggg)"
				+ "</html>";
	}

	@Override
	public ModControl getControlOption() {
		return ModControl.TOGGLE;
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public void onStart() {
		eventManager.addListener(TickEvent.class, this);
	}

	@Override
	public void onStop() {
		eventManager.removeListener(TickEvent.class, this);
	}

	@Override
	public int loop() {
		return 9000;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(Event event) {
		World world = minecraft.getWorld();
		if(world != null)
			for(Entity entity : (List<Entity>) world.getEntities())
				if(entity instanceof Chicken)
					((Chicken) entity).setEggDropTimer(0);
	}
}
