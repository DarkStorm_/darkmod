package org.darkstorm.minecraft.darkmod.mod.util.interfaces;

import org.darkstorm.minecraft.darkmod.mod.util.Location;

public interface Locatable {
	public Location getLocation();
}
