package org.darkstorm.minecraft.darkmod.access.injection;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.jar.*;

import javax.swing.*;

import org.apache.bcel.classfile.*;
import org.apache.bcel.generic.*;
import org.darkstorm.minecraft.darkmod.access.injection.hooks.*;
import org.darkstorm.minecraft.darkmod.access.injection.misc.*;
import org.darkstorm.minecraft.darkmod.tools.*;
import org.darkstorm.minecraft.darkmod.ui.LoginUI;
import org.darkstorm.tools.misc.SysTools;

public class Injector {
	private LoginUI loginUI;
	private String minecraftJarName;
	private List<String> entryNames;
	private ClassVector classes;
	private ClassLoader classLoader;
	private Hook[] hooks;

	private String version;

	private Map<String, String> interfaceToClassNamesMap = new HashMap<String, String>();

	public Injector(LoginUI loginUI) {
		this.loginUI = loginUI;
	}

	public void run() {
		if(loginUI != null) {
			JButton dialogButton1 = loginUI.getDialogButton1();
			JButton dialogButton2 = loginUI.getDialogButton2();
			JProgressBar progressBar = loginUI.getDialogProgressBar();
			dialogButton1.setVisible(false);
			dialogButton2.setVisible(false);
			progressBar.setVisible(true);
			progressBar.setValue(progressBar.getMinimum());
			progressBar.setIndeterminate(false);
		}
		loadJar();
		loadHooks();
		injectHooks();
		dumpJar();
		loadClasses();
	}

	private void loadJar() {
		try {
			JarFile minecraftJar = locateMinecraftJar();
			if(loginUI != null)
				loginUI.setDialogText("Loading jar...");
			minecraftJarName = minecraftJar.getName();
			classes = new ClassVector();
			entryNames = new Vector<String>();
			Vector<JarEntry> listedEntries = new Vector<JarEntry>();
			Enumeration<?> enumeratedEntries = minecraftJar.entries();
			while(enumeratedEntries.hasMoreElements()) {
				JarEntry entry = (JarEntry) enumeratedEntries.nextElement();
				listedEntries.add(entry);
			}
			JProgressBar progressBar = null;
			if(loginUI != null) {
				progressBar = loginUI.getDialogProgressBar();
				progressBar.setMinimum(0);
				progressBar.setMaximum(listedEntries.size());
			}
			int progress = 0;
			for(JarEntry entry : listedEntries) {
				entryNames.add(entry.getName());
				if(entry.getName().endsWith(".class")) {
					ClassParser entryClassParser = new ClassParser(minecraftJar.getInputStream(entry), entry.getName());
					JavaClass parsedClass = entryClassParser.parse();
					ClassGen classGen = new ClassGen(parsedClass);
					classes.add(classGen);
				}
				progress++;
				if(loginUI != null)
					progressBar.setValue(progress);
			}
		} catch(Exception exception) {
			exception.printStackTrace();
			SysTools.exit("Unable to load classes from jar", -1);
		}
	}

	private JarFile locateMinecraftJar() {
		if(loginUI != null) {
			loginUI.setDialogText("Locating minecraft directory...");
			loginUI.getDialogProgressBar().setIndeterminate(true);
		}
		File workingDir = Tools.getMinecraftDirectory();
		File versionsDir = new File(workingDir, "versions");
		File[] possibleVersions = versionsDir.listFiles();
		File bestVersion = null;
		for(File version : possibleVersions)
			if(bestVersion == null || compareVersions(version, bestVersion) > 0)
				bestVersion = version;
		if(bestVersion == null)
			throw new RuntimeException("Could not locate latest version!");
		version = bestVersion.getName();
		File minecraftFile = new File(bestVersion, version.concat(".jar"));
		if(!minecraftFile.exists())
			SysTools.exit("Minecraft jar does not exist", -1);
		minecraftJarName = minecraftFile.getAbsolutePath();
		if(loginUI != null)
			loginUI.getDialogProgressBar().setIndeterminate(false);
		try {
			return new JarFile(minecraftFile);
		} catch(IOException exception) {
			exception.printStackTrace();
			SysTools.exit("Unable to load minecraft jar", -1);
		}
		return null;
	}

	private int compareVersions(File version1, File version2) {
		String name1 = version1.getName(), name2 = version2.getName();
		if(!name1.matches("[0-9]+(\\.[0-9]+)*") || !name2.matches("[0-9]+(\\.[0-9]+)*"))
			throw new IllegalArgumentException();
		String[] parts1 = name1.split("\\."), parts2 = name2.split("\\.");
		for(int i = 0; i < parts1.length && i < parts2.length; i++) {
			int part1 = Integer.parseInt(parts1[i]), part2 = Integer.parseInt(parts2[i]);
			int compare = Integer.compare(part1, part2);
			if(compare != 0)
				return compare;
		}
		return Integer.compare(parts1.length, parts2.length);
	}

	private void loadHooks() {
		HookLoader hookLoader = new HookLoader(this);
		hooks = hookLoader.loadHooks(loginUI);
	}

	private void injectHooks() {
		JProgressBar progressBar = null;
		if(loginUI != null) {
			loginUI.setDialogText("Injecting hooks...");
			progressBar = loginUI.getDialogProgressBar();
			progressBar.setIndeterminate(false);
			progressBar.setMinimum(0);
			progressBar.setValue(0);
			progressBar.setMaximum(classes.size() * hooks.length);
		}
		int progress = 0;
		int successful = 0;
		for(ClassGen classGen : classes) {
			for(Hook hook : hooks) {
				if(hook instanceof InterfaceHook) {
					if(!hook.isInjected()) {
						try {
							hook.attemptInjection(classGen);
						} catch(Exception exception) {
							exception.printStackTrace();
							printFailedHook(hook);
						}
						if(hook.isInjected()) {
							String interfaceName = ((InterfaceHook) hook).getInterfaceName();
							interfaceToClassNamesMap.put(interfaceName, classGen.getClassName());
						}
					}
				}
				progress++;
				if(loginUI != null)
					progressBar.setValue(progress);
			}
		}
		Vector<Hook> hooks = new Vector<Hook>();
		if(loginUI != null) {
			progressBar.setValue(0);
			progressBar.setMaximum(classes.size() * hooks.size());
		}
		progress = 0;
		for(Hook hook : this.hooks) {
			if(!(hook instanceof InterfaceHook))
				hooks.add(hook);
		}
		for(ClassGen classGen : classes) {
			for(Hook hook : hooks) {
				try {
					hook.attemptInjection(classGen);
				} catch(Exception exception) {
					exception.printStackTrace();
					printFailedHook(hook);
				}
				progress++;
				if(loginUI != null)
					progressBar.setValue(progress);
			}
		}
		for(Hook hook : this.hooks)
			if(hook.isInjected())
				successful++;
		if(successful < this.hooks.length) {
			System.err.println("Failure to inject all hooks. Hooks injected: " + successful + ", hook count: " + this.hooks.length);
			if(loginUI != null) {
				loginUI.showError("<html><center>Failure injecting hooks.<br/>" + "Your jar may be modified to be<br/>" + "incompatible with DarkMod.</center></html>");
			} else
				System.exit(-1);
		}
	}

	private void printFailedHook(Hook hook) {
		String message = "Failed to inject ";
		if(hook instanceof InterfaceHook) {
			InterfaceHook interfaceHook = (InterfaceHook) hook;
			message += "interface " + interfaceHook.getInterfaceName();
		} else if(hook instanceof GetterHook) {
			GetterHook getterHook = (GetterHook) hook;
			message += "getter " + getterHook.getReturnType() + " " + getterHook.getInterfaceName() + "." + getterHook.getGetterName() + "()";
		} else if(hook instanceof SetterHook) {
			SetterHook setterHook = (SetterHook) hook;
			message += "setter " + setterHook.getInterfaceName() + "." + setterHook.getSetterName() + "(" + setterHook.getReturnType() + ")";
		} else if(hook instanceof MethodHook) {
			MethodHook methodHook = (MethodHook) hook;
			message += "method " + Type.getReturnType(methodHook.getNewMethodSignature()) + " " + methodHook.getInterfaceName() + "." + methodHook.getNewMethodName() + "(";
			Type[] argumentTypes = Type.getArgumentTypes(methodHook.getNewMethodSignature());
			if(argumentTypes.length > 0) {
				message += argumentTypes[0].toString();
				for(int i = 1; i < argumentTypes.length; i++)
					message += ", " + argumentTypes[i].toString();
			}
			message += ")";
		} else if(hook instanceof CallbackHook) {
			CallbackHook callbackHook = (CallbackHook) hook;
			message += "callback to " + callbackHook.getInterfaceName() + "." + callbackHook.getCallbackMethod() + "()";
		} else if(hook instanceof BytecodeHook) {
			BytecodeHook bytecodeHook = (BytecodeHook) hook;
			message += "bytecode to " + Type.getReturnType(bytecodeHook.getMethodSignature()) + " " + bytecodeHook.getClassName() + "." + bytecodeHook.getMethodName() + "(";
			Type[] argumentTypes = Type.getArgumentTypes(bytecodeHook.getMethodSignature());
			if(argumentTypes.length > 0) {
				message += argumentTypes[0].toString();
				for(int i = 1; i < argumentTypes.length; i++)
					message += ", " + argumentTypes[i].toString();
			}
			message += ")";
		} else
			message += "an unknown hook";
		System.err.println(message);
	}

	/**
	 * For debugging purposes
	 */
	private void dumpJar() {
		try {
			JProgressBar progressBar = null;
			if(loginUI != null) {
				loginUI.setDialogText("Outputting jar...");
				progressBar = loginUI.getDialogProgressBar();
				progressBar.setIndeterminate(false);
				progressBar.setMinimum(0);
				progressBar.setValue(0);
				progressBar.setMaximum(classes.size());
			}
			int progress = 0;
			File file = new File("api/minecraft.jar");
			FileOutputStream stream = new FileOutputStream(file);
			JarOutputStream out = new JarOutputStream(stream);
			for(ClassGen classGen : classes) {
				JarEntry jarEntry = new JarEntry(classGen.getClassName().replace('.', '/') + ".class");
				out.putNextEntry(jarEntry);
				out.write(classGen.getJavaClass().getBytes());
				progress++;
				if(loginUI != null)
					progressBar.setValue(progress);
			}
			out.close();
			stream.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void loadClasses() {
		try {
			JProgressBar progressBar = null;
			if(loginUI != null) {
				loginUI.setDialogText("Loading new jar...");
				progressBar = loginUI.getDialogProgressBar();
				progressBar.setIndeterminate(false);
				progressBar.setMinimum(0);
				progressBar.setValue(0);
				progressBar.setMaximum(classes.size());
			}
			int progress = 0;
			HashMap<String, byte[]> outputClasses = new HashMap<String, byte[]>();
			for(ClassGen classGen : classes) {
				JavaClass javaClass = classGen.getJavaClass();
				outputClasses.put(classGen.getClassName(), javaClass.getBytes());
				progress++;
				if(loginUI != null)
					progressBar.setValue(progress);
			}
			classLoader = new CustomClassLoader(minecraftJarName, outputClasses, entryNames, new URL[0]);
			initClassRepository();
		} catch(Throwable e) {
			e.printStackTrace();
			SysTools.exit("Unable to dump jar", -1);
		}
	}

	private void initClassRepository() {
		Map<Class<?>, Class<?>> interfaceToClassMap = new HashMap<Class<?>, Class<?>>();
		for(String interfaceName : interfaceToClassNamesMap.keySet()) {
			try {
				Class<?> interfaceClass = Class.forName(interfaceName);
				Class<?> minecraftClass = classLoader.loadClass(interfaceToClassNamesMap.get(interfaceName));
				interfaceToClassMap.put(interfaceClass, minecraftClass);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
		for(ClassGen classGen : this.classes)
			try {
				String className = classGen.getClassName();
				classes.put(className, classLoader.loadClass(className));
			} catch(ClassNotFoundException exception) {
				exception.printStackTrace();
			}
		ClassRepository.init(classes, interfaceToClassMap);
	}

	public ClassVector getClasses() {
		return classes;
	}

	public Hook[] getHooks() {
		return hooks;
	}

	public ClassLoader getClassLoader() {
		return classLoader;
	}

	public String getVersion() {
		return version;
	}
}
