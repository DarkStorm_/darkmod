import org.darkstorm.minecraft.darkmod.events.*;
import org.darkstorm.minecraft.darkmod.hooks.client.*;
import org.darkstorm.minecraft.darkmod.hooks.client.packets.Packet4UpdateTime;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.minecraft.darkmod.mod.commands.*;
import org.darkstorm.minecraft.darkmod.mod.util.constants.ChatColor;
import org.darkstorm.tools.events.*;
import org.darkstorm.tools.strings.StringTools;

public class TimeMod extends Mod implements CommandListener, EventListener {
	private boolean holdingTime, holdingWeather, raining, thundering;
	private long timeHeld;

	@Override
	public ModControl getControlOption() {
		return ModControl.TOGGLE;
	}

	@Override
	public String getName() {
		return "Time Mod";
	}

	@Override
	public String getShortDescription() {
		return "Adds /time to control time";
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public void onStart() {
		eventManager.addListener(PacketEvent.class, this);
		eventManager.addListener(TickEvent.class, this);
		commandManager.registerListener(new Command("time", "/time [hold|day|night|[+-*/](0-24000)]", "Time control options"), this);
		commandManager.registerListener(new Command("weather", "/weather [hold|rain|thunder|none]", "Weather control options"), this);
	}

	@Override
	public void onStop() {
		eventManager.removeListener(PacketEvent.class, this);
		eventManager.removeListener(TickEvent.class, this);
		commandManager.unregisterListener("time");
		commandManager.unregisterListener("weather");
	}

	@Override
	public int loop() throws InterruptedException {
		return 1000;
	}

	@Override
	public void onCommand(String command) {
		World world = minecraft.getWorld();
		if(world == null)
			return;
		WorldInfo worldInfo = world.getWorldInfo();
		String[] parts = command.split(" ");
		if(parts[0].equalsIgnoreCase("time")) {
			if(parts.length == 1) {
				displayText(ChatColor.GRAY + "Time is " + worldInfo.getTime() + ".");
			} else if(parts.length == 2) {
				if(parts[1].equalsIgnoreCase("hold")) {
					holdingTime = !holdingTime;
					timeHeld = worldInfo.getTime();
					if(holdingTime)
						displayText(ChatColor.GRAY + "Now holding time.");
					else
						displayText(ChatColor.GRAY + "No longer holding time.");
				} else if(parts[1].equalsIgnoreCase("day")) {
					worldInfo.setTime(0);
					displayText(ChatColor.GRAY + "It is now daytime.");
				} else if(parts[1].equalsIgnoreCase("night")) {
					worldInfo.setTime(14000);
					displayText(ChatColor.GRAY + "It is now nighttime.");
				} else if(!Character.isDigit(parts[1].charAt(0)) && StringTools.isLong(parts[1].substring(1))) {
					long time = Long.parseLong(parts[1].substring(1));
					switch(parts[1].charAt(0)) {
					case '+':
						worldInfo.setTime(worldInfo.getTime() + time);
						break;
					case '-':
						worldInfo.setTime(worldInfo.getTime() - time);
						break;
					case '*':
						worldInfo.setTime(worldInfo.getTime() * time);
						break;
					case '/':
						worldInfo.setTime(worldInfo.getTime() / time);
						break;
					}
				} else if(StringTools.isLong(parts[1])) {
					worldInfo.setTime(Long.parseLong(parts[1]));
					displayText(ChatColor.GRAY + "It is now " + worldInfo.getTime() + ".");
				}
				timeHeld = worldInfo.getTime();
			}
		} else if(parts[0].equalsIgnoreCase("weather")) {
			if(parts.length > 1) {
				if(parts[1].equalsIgnoreCase("hold")) {
					holdingWeather = !holdingWeather;
					if(holdingWeather)
						displayText(ChatColor.GRAY + "Now holding weather.");
					else
						displayText(ChatColor.GRAY + "No longer holding weather.");
				} else if(parts[1].equalsIgnoreCase("rain")) {
					worldInfo.setRaining(raining = !raining);
					worldInfo.setRainTime(raining ? 5000 : 0);
					if(raining)
						displayText(ChatColor.GRAY + "Now raining.");
					else
						displayText(ChatColor.GRAY + "No longer raining.");
				} else if(parts[1].equalsIgnoreCase("thunder")) {
					worldInfo.setThundering(thundering = !thundering);
					worldInfo.setThunderTime(thundering ? 5000 : 0);
					if(thundering)
						displayText(ChatColor.GRAY + "Now thundering.");
					else
						displayText(ChatColor.GRAY + "No longer thundering.");
				} else if(parts[1].equalsIgnoreCase("none")) {
					if(raining) {
						worldInfo.setRaining(raining = false);
						worldInfo.setRainTime(0);
						displayText(ChatColor.GRAY + "No longer raining.");
					}
					if(thundering) {
						worldInfo.setThundering(thundering = false);
						worldInfo.setThunderTime(0);
						displayText(ChatColor.GRAY + "No longer thundering.");
					}
				}
			} else {
				if(worldInfo.isRaining())
					displayText(ChatColor.GRAY + "It is currently raining for " + worldInfo.getRainTime() + " ticks.");
				else
					displayText(ChatColor.GRAY + "It is not currently raining.");
				if(worldInfo.isThundering())
					displayText(ChatColor.GRAY + "It is currently thundering for " + worldInfo.getThunderTime() + " ticks.");
				else
					displayText(ChatColor.GRAY + "It is not currently thundering.");
			}
		}
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof PacketReceivedEvent) {
			PacketEvent packetEvent = (PacketEvent) event;
			Packet packet = packetEvent.getPacket();
			if(packet instanceof Packet4UpdateTime) {
				Packet4UpdateTime timePacket = (Packet4UpdateTime) packet;
				if(holdingTime) {
					World world = minecraft.getWorld();
					if(world == null)
						return;
					WorldInfo worldInfo = world.getWorldInfo();
					if(worldInfo == null)
						return;
					timePacket.setTime(timeHeld);
				} else
					timeHeld = timePacket.getTime();
			}
		} else if(event instanceof TickEvent) {
			World world = minecraft.getWorld();
			if(world == null)
				return;
			WorldInfo worldInfo = world.getWorldInfo();
			if(worldInfo == null)
				return;
			if(holdingTime)
				worldInfo.setTime(timeHeld);
			else
				timeHeld = worldInfo.getTime();
			if(holdingWeather) {
				worldInfo.setRaining(raining);
				worldInfo.setThundering(thundering);
			} else {
				raining = worldInfo.isRaining();
				thundering = worldInfo.isThundering();
			}
		}
	}
}
