package org.darkstorm.minecraft.darkmod.access.injection.hooks;

import org.darkstorm.minecraft.darkmod.access.injection.Injector;
import org.jdom.Element;

import org.apache.bcel.generic.ClassGen;

public class InterfaceHook extends Hook {
	private String className;
	private String interfaceName;
	private String[] interfaces;

	public InterfaceHook(Injector injector, Element element) {
		super(injector, element);
	}

	public String getClassName() {
		return className;
	}

	@Override
	public String getInterfaceName() {
		return interfaceName;
	}

	public String[] getInterfaces() {
		return interfaces;
	}

	@Override
	protected void readElement(Element element) {
		className = element.getAttributeValue("class");
		interfaceName = element.getAttributeValue("interface");
		String interfaceNames = element.getAttributeValue("interfaces");
		if(interfaceNames != null)
			interfaces = interfaceNames.split(",");
		else
			interfaces = new String[0];
	}

	@Override
	protected boolean isInjectable(ClassGen classGen) {
		return className.equals(classGen.getClassName());
	}

	@Override
	protected void inject(ClassGen classGen) {
		classGen.addInterface(interfaceName);
	}
}
