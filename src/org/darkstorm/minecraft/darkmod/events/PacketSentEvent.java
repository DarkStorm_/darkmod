package org.darkstorm.minecraft.darkmod.events;

import org.darkstorm.minecraft.darkmod.hooks.client.Packet;

public class PacketSentEvent extends PacketEvent {
	private boolean cancelled = false;

	public PacketSentEvent(Packet packet) {
		super(packet);
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}
