import org.darkstorm.minecraft.darkmod.hooks.client.*;
import org.darkstorm.minecraft.darkmod.hooks.client.packets.Packet13PlayerMoveRotate;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.minecraft.darkmod.mod.commands.*;
import org.darkstorm.minecraft.darkmod.tools.*;

public class SpawnTPMod extends Mod implements CommandListener {

	@Override
	public ModControl getControlOption() {
		return ModControl.NONE;
	}

	@Override
	public void onStart() {
		commandManager
				.registerListener(
						new Command("spawntp", "/spawntp",
								"Teleport you to spawn and kick you from the server (on Bukkit)"),
						this);
	}

	@Override
	public void onStop() {
		commandManager.unregisterListener("spawntp");
	}

	@Override
	public String getFullDescription() {
		return "";
	}

	@Override
	public String getName() {
		return "Spawn TP";
	}

	@Override
	public String getShortDescription() {
		return "";
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public int loop() {
		return 90000;
	}

	@Override
	public void onCommand(String command) {
		if(!(minecraft.getPlayer() instanceof MultiplayerPlayer))
			return;
		String[] parts = command.split(" ");
		if(parts[0].equalsIgnoreCase("spawntp")) {
			MultiplayerWorld world = minecraft.getWorld();
			NetworkHandler networkHandler = world.getNetworkHandler();
			Packet13PlayerMoveRotate movePacket = ReflectionUtil
					.instantiate(ClassRepository
							.getClassForInterface(Packet13PlayerMoveRotate.class));
			movePacket.setX(Double.NaN);
			movePacket.setStance(Double.NaN);
			movePacket.setZ(Double.NaN);
			movePacket.setRotationX(0);
			movePacket.setRotationY(0);
			movePacket.setY(Double.NaN);
			networkHandler.sendPacket(movePacket);
		}
	}
}
