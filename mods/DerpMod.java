import java.util.Random;

import org.darkstorm.minecraft.darkmod.events.*;
import org.darkstorm.minecraft.darkmod.hooks.client.Player;
import org.darkstorm.minecraft.darkmod.hooks.client.packets.Packet10PlayerPosition;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.minecraft.darkmod.mod.commands.*;
import org.darkstorm.minecraft.darkmod.mod.util.constants.ChatColor;
import org.darkstorm.tools.events.Event;

public class DerpMod extends Mod implements CommandListener {
	private Random random = new Random();
	private boolean derp = false;

	private boolean lastMovement = false;

	@Override
	public int loop() throws InterruptedException {
		Player player = minecraft.getPlayer();
		if(player == null)
			return 500;
		player.setRotationY(player.getRotationY()
				+ (lastMovement ? -0.01F : 0.01F));
		lastMovement = !lastMovement;
		return derp ? 50 : 500;
	}

	@Override
	public String getName() {
		return "Derp Mod";
	}

	@Override
	public String getShortDescription() {
		return "";
	}

	@Override
	public ModControl getControlOption() {
		return ModControl.NONE;
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public void onStart() {
		commandManager.registerListener(new Command("derp", "/derp",
				"Enters or leaves derp mode"), this);
		eventManager.addListener(PacketEvent.class, this);
	}

	@Override
	public void onStop() {
		commandManager.unregisterListener("derp");
		eventManager.removeListener(PacketEvent.class, this);
	}

	@Override
	public void onCommand(String command) {
		if(command.equalsIgnoreCase("derp")) {
			derp = !derp;
			displayText(ChatColor.GRAY + "Derp mode " + ChatColor.GOLD
					+ (derp ? "activateeeeehuurr" : "deactivate"));
		}
	}

	@Override
	public void onEvent(Event event) {
		if(!derp)
			return;
		if(event instanceof PacketEvent) {
			if(((PacketEvent) event).getPacket() instanceof Packet10PlayerPosition) {
				Packet10PlayerPosition packet = (Packet10PlayerPosition) ((PacketEvent) event)
						.getPacket();
				if(event instanceof PacketSentEvent) {
					packet.setRotationX(random.nextInt(360));
					packet.setRotationY(random.nextInt(360));
				} else {
					Player player = minecraft.getPlayer();
					if(player == null)
						return;
					packet.setRotationX(player.getRotationX());
					packet.setRotationY(player.getRotationY());
				}
			}
		}
	}
}
