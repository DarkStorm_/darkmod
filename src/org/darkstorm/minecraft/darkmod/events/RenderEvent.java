package org.darkstorm.minecraft.darkmod.events;

import org.darkstorm.tools.events.Event;

public class RenderEvent extends Event {
	public static final int PRE_RENDER = 0, RENDER = 1, GAME_RENDER = 2,
			POST_RENDER = 3, RENDER_ENTITIES = 4, RENDER_ENTITIES_END = 5,
			RENDER_ENTITIES_PREPARE = 6, RENDER_ENTITIES_GLOBAL = 7,
			RENDER_ENTITIES_TILE_ENTITIES = 8, UNKNOWN = 99;

	public RenderEvent(int status) {
		super(status);
	}

	public int getStatus() {
		return (Integer) info[0];
	}
}
