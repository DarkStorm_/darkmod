import org.darkstorm.minecraft.darkmod.DarkMod;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.minecraft.darkmod.mod.commands.*;

public class NameChangeMod extends Mod implements CommandListener {
	@Override
	public void onStart() {
		commandManager.registerListener(new Command("name", "/name [username]",
				"Changes your name"), this);
	}

	@Override
	public void onStop() {
		commandManager.unregisterListener("name");
	}

	@Override
	public int loop() throws InterruptedException {
		return 9000;
	}

	@Override
	public String getName() {
		return "Name Change Mod";
	}

	@Override
	public String getShortDescription() {
		return "Changes your username with /name";
	}

	@Override
	public ModControl getControlOption() {
		return ModControl.NONE;
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public void onCommand(String command) {
		String[] parts = command.split(" ");
		if(parts[0].equalsIgnoreCase("name") && parts.length == 2) {
			String username = parts[1];
			DarkMod.getInstance().setUsername(username);
		}
	}
}
