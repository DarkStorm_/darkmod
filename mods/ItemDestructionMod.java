import java.util.List;

import org.darkstorm.minecraft.darkmod.hooks.client.*;
import org.darkstorm.minecraft.darkmod.hooks.client.packets.Packet7UseEntity;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.minecraft.darkmod.mod.util.Location;
import org.darkstorm.minecraft.darkmod.tools.*;

public class ItemDestructionMod extends Mod {

	@Override
	public ModControl getControlOption() {
		return ModControl.TOGGLE;
	}

	@Override
	public String getName() {
		return "Item Destruction Mod";
	}

	@Override
	public String getShortDescription() {
		return "Destroys nearby items";
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public void onStart() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public int loop() throws InterruptedException {
		try {
			if(minecraft.getWorld() == null
					|| !(minecraft.getWorld() instanceof MultiplayerWorld))
				return 5000;
			Player player = minecraft.getPlayer();
			int playerID = player.getID();
			Location playerLocation = new Location(player.getX(),
					player.getY(), player.getZ());
			MultiplayerWorld world = minecraft.getWorld();
			NetworkHandler networkHandler = world.getNetworkHandler();
			try {
				for(Entity entity : (List<Entity>) world.getEntities()) {
					if(entity.equals(player) || entity instanceof Animable)
						continue;
					Location entityLocation = new Location(entity.getX(),
							entity.getY(), entity.getZ());
					if(getDistanceBetween(playerLocation, entityLocation) > 5)
						continue;
					int id = entity.getID();
					Packet7UseEntity attackPacket = (Packet7UseEntity) ReflectionUtil
							.instantiate(ClassRepository
									.getClassForInterface(Packet7UseEntity.class));
					attackPacket.setEntityID(playerID);
					attackPacket.setTargetEntityID(id);
					attackPacket.setButton(1);
					networkHandler.sendPacket(attackPacket);
				}
			} catch(Throwable exception) {
				exception.printStackTrace();
			}
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		return 500;
	}
}
