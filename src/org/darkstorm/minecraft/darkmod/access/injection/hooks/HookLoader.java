package org.darkstorm.minecraft.darkmod.access.injection.hooks;

import java.io.File;
import java.net.URL;
import java.util.*;

import javax.swing.JProgressBar;

import org.darkstorm.minecraft.darkmod.DarkMod;
import org.darkstorm.minecraft.darkmod.access.injection.Injector;
import org.darkstorm.minecraft.darkmod.tools.Tools;
import org.darkstorm.minecraft.darkmod.ui.LoginUI;
import org.darkstorm.tools.misc.SysTools;
import org.jdom.*;
import org.jdom.input.SAXBuilder;

public class HookLoader {
	private Injector injector;

	private String version;

	public HookLoader(Injector injector) {
		this.injector = injector;
	}

	public Hook[] loadHooks(LoginUI loginUI) {
		if(loginUI != null) {
			loginUI.setDialogText("Loading hooks...");
			loginUI.getDialogProgressBar().setIndeterminate(true);
		}
		Document document = loadXML(loginUI);
		if(loginUI != null)
			loginUI.getDialogProgressBar().setIndeterminate(false);
		return parseHooks(document, loginUI);
	}

	private Document loadXML(LoginUI loginUI) {
		Exception exception1 = null, exception2 = null;
		try {
			DarkMod darkMod = DarkMod.getInstance();
			if(Tools.isRunningFromJar() && !darkMod.isPlayingOffline())
				return new SAXBuilder().build(new URL("http://darkstorm652.webs.com/darkmod/Hooks (" + Tools.getMinecraftBuild() + ").xml"));
		} catch(Exception exception) {
			exception1 = exception;
		}
		try {
			return new SAXBuilder().build(new File("Hooks.xml"));
		} catch(Exception exception) {
			exception2 = exception;
		}
		try {
			return new SAXBuilder().build(getClass().getResource("/Hooks.xml"));
		} catch(Exception exception) {
			if(exception1 != null)
				exception1.printStackTrace();
			if(exception2 != null)
				exception2.printStackTrace();
			exception.printStackTrace();
			if(loginUI != null)
				loginUI.showMessage("Unable to download hooks!");
			else
				SysTools.exit("Unable to load xml file!", -1);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private Hook[] parseHooks(Document document, LoginUI loginUI) {
		List<Hook> hooks = new ArrayList<Hook>();
		Element rootElement = document.getRootElement();
		version = rootElement.getAttributeValue("version");
		List<Element> hookElements = rootElement.getChildren();
		JProgressBar progressBar = null;
		if(loginUI != null) {
			loginUI.setDialogText("Parsing hook data...");
			progressBar = loginUI.getDialogProgressBar();
			progressBar.setMinimum(0);
			progressBar.setMaximum(hookElements.size());
			progressBar.setValue(0);
		}
		int progress = 0;
		for(Element element : hookElements) {
			String type = element.getName();
			if(type.equals("interface")) {
				hooks.add(new InterfaceHook(injector, element));
				for(Element interfaceElement : (List<Element>) element.getChildren()) {
					type = interfaceElement.getName();
					if(type.equals("getter"))
						hooks.add(new GetterHook(injector, interfaceElement));
					else if(type.equals("setter"))
						hooks.add(new SetterHook(injector, interfaceElement));
					else if(type.equals("method"))
						hooks.add(new MethodHook(injector, interfaceElement));
					else
						SysTools.exit("Error loading hooks: invalid type", -1);
				}
			} else if(type.equals("callback"))
				hooks.add(new CallbackHook(injector, element));
			else if(type.equals("bytecode"))
				hooks.add(new BytecodeHook(injector, element));
			else
				SysTools.exit("Error loading hooks: invalid type", -1);
			progress++;
			if(loginUI != null)
				progressBar.setValue(progress);
		}
		return hooks.toArray(new Hook[hooks.size()]);
	}

	public Injector getInjector() {
		return injector;
	}

	public String getVersion() {
		return version;
	}
}