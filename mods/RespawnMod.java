import org.darkstorm.minecraft.darkmod.hooks.client.MultiplayerWorld;
import org.darkstorm.minecraft.darkmod.hooks.client.packets.Packet205ClientStatus;
import org.darkstorm.minecraft.darkmod.mod.Mod;
import org.darkstorm.minecraft.darkmod.mod.commands.*;
import org.darkstorm.minecraft.darkmod.tools.*;

public class RespawnMod extends Mod implements CommandListener {

	@Override
	public ModControl getControlOption() {
		return ModControl.NONE;
	}

	@Override
	public String getName() {
		return "Respawn Mod";
	}

	@Override
	public String getShortDescription() {
		return "Adds /respawn command to the game";
	}

	@Override
	public boolean hasOptions() {
		return false;
	}

	@Override
	public int loop() throws InterruptedException {
		return 9000;
	}

	@Override
	public void onStart() {
		commandManager.registerListener(new Command("respawn", "/respawn",
				"Causes you to respawn"), this);
	}

	@Override
	public void onStop() {
		commandManager.unregisterListener("respawn");
	}

	@Override
	public void onCommand(String command) {
		if(command.equalsIgnoreCase("respawn")
				&& minecraft.getWorld() instanceof MultiplayerWorld) {
			Packet205ClientStatus status = (Packet205ClientStatus) ReflectionUtil
					.instantiate(ClassRepository
							.getClassForInterface(Packet205ClientStatus.class));
			status.setAction(1);
			minecraft.getWorld().getNetworkHandler().sendPacket(status);
		}
	}

}
