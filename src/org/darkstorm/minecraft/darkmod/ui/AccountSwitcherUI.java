package org.darkstorm.minecraft.darkmod.ui;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;

import javax.imageio.*;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

import org.darkstorm.minecraft.darkmod.DarkMod;
import org.darkstorm.minecraft.darkmod.tools.LoginUtil;
import org.darkstorm.tools.settings.*;
import org.w3c.dom.*;

@SuppressWarnings("serial")
public class AccountSwitcherUI extends JDialog {
	// JFormDesigner - Variables declaration - DO NOT MODIFY
	// //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JScrollPane scrollPane1;
	private JList accountList;
	private JPanel buttonBar;
	private JButton closeButton;
	private JButton loginButton;
	private JPopupMenu popupMenu;
	private JMenuItem loginMenuItem;
	private JMenuItem editMenuItem;
	private JMenuItem deleteMenuItem;
	private JMenuItem newMenuItem;

	// JFormDesigner - End of variables declaration //GEN-END:variables

	private String loadingUsername, loggedInUsername;
	private ExecutorService service = Executors.newSingleThreadExecutor();
	private Future<?> loginThread;

	public AccountSwitcherUI() {
		super(DarkMod.getInstance().getUI());
		setModalityType(ModalityType.APPLICATION_MODAL);
		initComponents();
		accountList.setModel(new DefaultListModel());
		updateListData();
		accountList.setCellRenderer(new IconListRenderer(accountList));
	}

	private void updateListData() {
		DarkMod darkMod = DarkMod.getInstance();
		SettingsHandler settingsHandler = darkMod.getSettingsHandler();
		SettingVector settings = settingsHandler.getSettings();
		Setting accountsSetting = settings.getSetting("accounts");
		DefaultListModel model = (DefaultListModel) accountList.getModel();
		int selectedIndex = accountList.getSelectedIndex();
		model.removeAllElements();
		if(accountsSetting != null) {
			for(Setting accountSetting : accountsSetting.getSubSettings()) {
				model.addElement(accountSetting.getKey());
			}
		}
		if(selectedIndex < model.getSize())
			accountList.setSelectedIndex(selectedIndex);
	}

	private class IconListRenderer extends DefaultListCellRenderer {
		private Icon checkIcon;
		private ImageIcon[] loadingFrames;
		private int index = 0;

		private IconListRenderer(final JList list) {
			checkIcon = new ImageIcon(getClass().getResource(
					"/resources/apply.png"));
			try {
				List<BufferedImage> frames = getFrames("/resources/loading.gif");
				loadingFrames = new ImageIcon[frames.size()];
				for(int i = 0; i < loadingFrames.length; i++)
					loadingFrames[i] = new ImageIcon(frames.get(i));
			} catch(IOException exception) {
				exception.printStackTrace();
			}
			final Timer timer = new Timer(100, null);
			ActionListener actionListener = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					index++;
					if(index >= loadingFrames.length) {
						index = 0;
					}

					if(isVisible() && loadingUsername != null)
						updateListData();
				}
			};
			timer.addActionListener(actionListener);
			timer.setRepeats(true);
			timer.start();
		}

		@Override
		public Component getListCellRendererComponent(final JList list,
				Object value, int index, boolean isSelected,
				boolean cellHasFocus) {

			// Get the renderer component from parent class

			final JLabel label = (JLabel) super.getListCellRendererComponent(
					list, value, index, isSelected, cellHasFocus);

			// Get icon to use for the list item value

			// Set icon to display for value

			if(loadingUsername != null
					&& label.getText().equals(loadingUsername))
				label.setIcon(loadingFrames[this.index]);
			else if(loggedInUsername != null
					&& label.getText().equals(loggedInUsername))
				label.setIcon(checkIcon);
			else
				label.setIcon(null);

			return label;
		}

		private List<BufferedImage> getFrames(String gif) throws IOException {
			List<BufferedImage> frames = new ArrayList<BufferedImage>();
			String[] imageatt = new String[] { "imageLeftPosition",
					"imageTopPosition", "imageWidth", "imageHeight" };

			ImageReader reader = ImageIO.getImageReadersByFormatName("gif")
					.next();
			ImageInputStream ciis = ImageIO.createImageInputStream(getClass()
					.getResourceAsStream(gif));
			reader.setInput(ciis, false);

			int noi = reader.getNumImages(true);

			BufferedImage master = null;

			for(int i = 0; i < noi; i++) {

				BufferedImage image = reader.read(i);
				IIOMetadata metadata = reader.getImageMetadata(i);

				Node tree = metadata.getAsTree("javax_imageio_gif_image_1.0");

				NodeList children = tree.getChildNodes();

				for(int j = 0; j < children.getLength(); j++) {

					Node nodeItem = children.item(j);

					if(nodeItem.getNodeName().equals("ImageDescriptor")) {

						Map<String, Integer> imageAttr = new HashMap<String, Integer>();

						for(int k = 0; k < imageatt.length; k++) {

							NamedNodeMap attr = nodeItem.getAttributes();

							Node attnode = attr.getNamedItem(imageatt[k]);

							imageAttr.put(imageatt[k],
									Integer.valueOf(attnode.getNodeValue()));

						}

						if(i == 0) {
							master = new BufferedImage(
									imageAttr.get("imageWidth"),
									imageAttr.get("imageHeight"),
									BufferedImage.TYPE_INT_ARGB);

						}
						master.getGraphics().drawImage(image,
								imageAttr.get("imageLeftPosition"),
								imageAttr.get("imageTopPosition"), null);

					}
				}
				frames.add(image);
			}
			return frames;
		}
	}

	private class AccountEditor extends JDialog {
		// JFormDesigner - Variables declaration - DO NOT MODIFY
		// //GEN-BEGIN:variables
		private JPanel dialogPane;
		private JPanel contentPanel;
		private JTextField usernameField;
		private JPasswordField passwordField;
		private JPanel buttonBar;
		private JButton cancelButton;
		private JButton saveButton;
		// JFormDesigner - End of variables declaration //GEN-END:variables

		private String editingName;

		private AccountEditor() {
			super(AccountSwitcherUI.this);
			setModalityType(ModalityType.APPLICATION_MODAL);
			initComponents();
			setSize(300, getSize().height);
			setLocationRelativeTo(getOwner());
			setVisible(true);
		}

		private AccountEditor(String username, String password) {
			super(AccountSwitcherUI.this);
			setModalityType(ModalityType.APPLICATION_MODAL);
			initComponents();
			setSize(300, getSize().height);
			setLocationRelativeTo(getOwner());
			editingName = username;
			usernameField.setText(username);
			passwordField.setText(password);
			setVisible(true);
		}

		private void cancelButtonActionPerformed(ActionEvent e) {
			setVisible(false);
			dispose();
		}

		private void saveButtonActionPerformed(ActionEvent e) {
			setVisible(false);
			dispose();
			DarkMod darkMod = DarkMod.getInstance();
			SettingsHandler settingsHandler = darkMod.getSettingsHandler();
			SettingVector settings = settingsHandler.getSettings();
			Setting accountsSetting = settings.getSetting("accounts");
			if(accountsSetting == null) {
				accountsSetting = new Setting("accounts", "");
				settings.add(accountsSetting);
			}
			SettingVector accountsSettings = accountsSetting.getSubSettings();
			int insertionPoint = accountsSettings.size();
			if(editingName != null) {
				Setting account = accountsSettings.getSetting(editingName);
				if(account != null) {
					insertionPoint = accountsSettings.indexOf(account);
					accountsSettings.remove(account);
				}
			}
			accountsSettings.insertElementAt(
					new Setting(usernameField.getText(), new String(
							passwordField.getPassword())), insertionPoint);
			updateListData();
		}

		private void initComponents() {
			// JFormDesigner - Component initialization - DO NOT MODIFY
			// //GEN-BEGIN:initComponents
			dialogPane = new JPanel();
			contentPanel = new JPanel();
			JLabel usernameLabel = new JLabel();
			usernameField = new JTextField();
			JLabel passwordLabel = new JLabel();
			passwordField = new JPasswordField();
			buttonBar = new JPanel();
			cancelButton = new JButton();
			saveButton = new JButton();

			// ======== this ========
			setTitle("Edit Account");
			Container contentPane = getContentPane();
			contentPane.setLayout(new BorderLayout());

			// ======== dialogPane ========
			{
				dialogPane.setBorder(new EmptyBorder(5, 5, 5, 5));
				dialogPane.setLayout(new BorderLayout());

				// ======== contentPanel ========
				{
					contentPanel.setLayout(new GridBagLayout());
					((GridBagLayout) contentPanel.getLayout()).columnWidths = new int[] {
							0, 0, 0 };
					((GridBagLayout) contentPanel.getLayout()).rowHeights = new int[] {
							0, 0, 0 };
					((GridBagLayout) contentPanel.getLayout()).columnWeights = new double[] {
							0.0, 1.0, 1.0E-4 };
					((GridBagLayout) contentPanel.getLayout()).rowWeights = new double[] {
							0.0, 0.0, 1.0E-4 };

					// ---- usernameLabel ----
					usernameLabel.setText("Username:");
					contentPanel.add(usernameLabel, new GridBagConstraints(0,
							0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0,
							0));
					contentPanel.add(usernameField, new GridBagConstraints(1,
							0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0,
							0));

					// ---- passwordLabel ----
					passwordLabel.setText("Password:");
					contentPanel.add(passwordLabel, new GridBagConstraints(0,
							1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0,
							0));
					contentPanel.add(passwordField, new GridBagConstraints(1,
							1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,
							0));
				}
				dialogPane.add(contentPanel, BorderLayout.CENTER);

				// ======== buttonBar ========
				{
					buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
					buttonBar.setLayout(new GridBagLayout());
					((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] {
							0, 85, 80 };
					((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] {
							1.0, 0.0, 0.0 };

					// ---- cancelButton ----
					cancelButton.setText("Cancel");
					cancelButton.setIcon(new ImageIcon(getClass().getResource(
							"/resources/cancel.png")));
					cancelButton.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							cancelButtonActionPerformed(e);
						}
					});
					buttonBar.add(cancelButton, new GridBagConstraints(1, 0, 1,
							1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0,
							0));

					// ---- saveButton ----
					saveButton.setText("Save");
					saveButton.setIcon(new ImageIcon(getClass().getResource(
							"/resources/apply.png")));
					saveButton.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							saveButtonActionPerformed(e);
						}
					});
					buttonBar.add(saveButton, new GridBagConstraints(2, 0, 1,
							1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,
							0));
				}
				dialogPane.add(buttonBar, BorderLayout.SOUTH);
			}
			contentPane.add(dialogPane, BorderLayout.CENTER);
			pack();
			// JFormDesigner - End of component initialization
			// //GEN-END:initComponents
		}
	}

	private void loginButtonActionPerformed(ActionEvent e) {
		login();
	}

	private Setting getSelectedAccount() {
		String selectedUsername = (String) accountList.getSelectedValue();
		if(selectedUsername == null)
			return null;
		DarkMod darkMod = DarkMod.getInstance();
		SettingsHandler settingsHandler = darkMod.getSettingsHandler();
		SettingVector settings = settingsHandler.getSettings();
		Setting accountsSetting = settings.getSetting("accounts");
		if(accountsSetting == null)
			return null;
		return accountsSetting.getSubSettings().getSetting(selectedUsername);
	}

	private void closeButtonActionPerformed(ActionEvent e) {
		setVisible(false);
	}

	private void newMenuItemActionPerformed(ActionEvent e) {
		new AccountEditor();
	}

	private void deleteMenuItemActionPerformed(ActionEvent e) {
		Setting account = getSelectedAccount();
		if(account == null)
			return;
		DarkMod darkMod = DarkMod.getInstance();
		SettingsHandler settingsHandler = darkMod.getSettingsHandler();
		SettingVector settings = settingsHandler.getSettings();
		Setting accountsSetting = settings.getSetting("accounts");
		if(accountsSetting == null)
			return;
		accountsSetting.getSubSettings().remove(account);
		updateListData();
	}

	private void editMenuItemActionPerformed(ActionEvent e) {
		Setting account = getSelectedAccount();
		if(account == null)
			return;
		new AccountEditor(account.getKey(), account.getValue());
	}

	private void loginMenuItemActionPerformed(ActionEvent e) {
		login();
	}

	private void login() {
		final Setting account = getSelectedAccount();
		if(account == null)
			return;

		if(loginThread != null)
			loginThread.cancel(true);
		loginThread = null;

		loginThread = service.submit(new Runnable() {
			@Override
			public void run() {
				DarkMod darkMod = DarkMod.getInstance();
				loadingUsername = account.getKey();
				updateListData();
				LoginUtil loginUtil = new LoginUtil();
				String result = loginUtil.login(account.getKey(),
						account.getValue());
				if(Thread.interrupted() || loadingUsername == null
						|| !account.getKey().equals(loadingUsername))
					return;
				if(loginUtil.isLoggedIn()) {
					darkMod.setUsername(loginUtil.getUsername());
					darkMod.setPassword(loginUtil.getPassword());
					darkMod.setSessionID(loginUtil.getSessionID());
					loadingUsername = null;
					loggedInUsername = account.getKey();
					updateListData();
				} else {
					loadingUsername = null;
					updateListData();
					JOptionPane.showMessageDialog(AccountSwitcherUI.this,
							"Unable to login: " + result, "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	private void accountListMousePressed(MouseEvent e) {
		if(e.isPopupTrigger()) {
			Setting account = getSelectedAccount();
			if(account == null) {
				loginMenuItem.setEnabled(false);
				editMenuItem.setEnabled(false);
				deleteMenuItem.setEnabled(false);
			} else {
				loginMenuItem.setEnabled(true);
				editMenuItem.setEnabled(true);
				deleteMenuItem.setEnabled(true);
			}
			popupMenu.show(e.getComponent(), e.getX(), e.getY());
		} else
			popupMenu.setVisible(false);
	}

	public JPanel getDialogPane() {
		return dialogPane;
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY
		// //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		scrollPane1 = new JScrollPane();
		accountList = new JList();
		buttonBar = new JPanel();
		closeButton = new JButton();
		loginButton = new JButton();
		popupMenu = new JPopupMenu();
		loginMenuItem = new JMenuItem();
		editMenuItem = new JMenuItem();
		deleteMenuItem = new JMenuItem();
		newMenuItem = new JMenuItem();

		// ======== this ========
		setTitle("Accounts");
		setIconImage(new ImageIcon(getClass().getResource(
				"/resources/accounts.png")).getImage());
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// ======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			dialogPane.setLayout(new BorderLayout());

			// ======== contentPanel ========
			{
				contentPanel.setLayout(new BorderLayout());

				// ======== scrollPane1 ========
				{

					// ---- accountList ----
					accountList.addMouseListener(new MouseAdapter() {
						@Override
						public void mousePressed(MouseEvent e) {
							accountListMousePressed(e);
						}
					});
					scrollPane1.setViewportView(accountList);
				}
				contentPanel.add(scrollPane1, BorderLayout.CENTER);
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			// ======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(5, 0, 0, 0));
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] {
						0, 85, 80 };
				((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] {
						1.0, 0.0, 0.0 };

				// ---- closeButton ----
				closeButton.setText("Close");
				closeButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						closeButtonActionPerformed(e);
					}
				});
				buttonBar.add(closeButton, new GridBagConstraints(1, 0, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

				// ---- loginButton ----
				loginButton.setText("Login");
				loginButton.setIcon(new ImageIcon(getClass().getResource(
						"/resources/apply.png")));
				loginButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						loginButtonActionPerformed(e);
					}
				});
				buttonBar.add(loginButton, new GridBagConstraints(2, 0, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		setSize(300, 300);
		setLocationRelativeTo(getOwner());

		// ======== popupMenu ========
		{

			// ---- loginMenuItem ----
			loginMenuItem.setText("Login");
			loginMenuItem.setIcon(new ImageIcon(getClass().getResource(
					"/resources/apply.png")));
			loginMenuItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					loginMenuItemActionPerformed(e);
				}
			});
			popupMenu.add(loginMenuItem);

			// ---- editMenuItem ----
			editMenuItem.setText("Edit");
			editMenuItem.setIcon(new ImageIcon(getClass().getResource(
					"/resources/edit.png")));
			editMenuItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					editMenuItemActionPerformed(e);
				}
			});
			popupMenu.add(editMenuItem);

			// ---- deleteMenuItem ----
			deleteMenuItem.setText("Delete");
			deleteMenuItem.setIcon(new ImageIcon(getClass().getResource(
					"/resources/remove.png")));
			deleteMenuItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					deleteMenuItemActionPerformed(e);
				}
			});
			popupMenu.add(deleteMenuItem);
			popupMenu.addSeparator();

			// ---- newMenuItem ----
			newMenuItem.setText("New");
			newMenuItem.setIcon(new ImageIcon(getClass().getResource(
					"/resources/add.png")));
			newMenuItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					newMenuItemActionPerformed(e);
				}
			});
			popupMenu.add(newMenuItem);
		}
		// //GEN-END:initComponents
	}
}
